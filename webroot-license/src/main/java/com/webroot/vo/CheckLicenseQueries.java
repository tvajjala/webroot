package com.webroot.vo;

import java.io.Serializable;

/**
 *
 * @author ThirupathiReddy V
 *
 */
public class CheckLicenseQueries implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1515284166712956600L;

    private CheckLicenseEx checklicenseex;

    public CheckLicenseEx getChecklicenseex() {
        return checklicenseex;
    }

    public void setChecklicenseex(CheckLicenseEx checklicenseex) {
        this.checklicenseex = checklicenseex;
    }

}
