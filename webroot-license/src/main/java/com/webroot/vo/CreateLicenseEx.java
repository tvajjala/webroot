package com.webroot.vo;

import java.io.Serializable;

/**
 *
 * @author ThirupathiReddy V
 *
 */
public class CreateLicenseEx implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4964073570442638664L;

    private int success;
    private String errormsg;

    private String oemid;

    private String deviceid;

    private String uid;

    private String provisioned_date;

    private String expiry_date;

    public String getOemid() {
        return oemid;
    }

    public void setOemid(String oemid) {
        this.oemid = oemid;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getProvisioned_date() {
        return provisioned_date;
    }

    public void setProvisioned_date(String provisioned_date) {
        this.provisioned_date = provisioned_date;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public void setExpiry_date(String expiry_date) {
        this.expiry_date = expiry_date;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public String getErrormsg() {
        return errormsg;
    }

    public void setErrormsg(String errormsg) {
        this.errormsg = errormsg;
    }

}
