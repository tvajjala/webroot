package com.webroot.vo;

import java.io.Serializable;

/**
 *
 * @author ThirupathiReddy V
 *
 */
public class CreateLicenseQueries implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2089365361872634904L;

    private CreateLicenseEx createlicenseex;

    public CreateLicenseEx getCreatelicenseex() {
        return createlicenseex;
    }

    public void setCreatelicenseex(CreateLicenseEx createlicenseex) {
        this.createlicenseex = createlicenseex;
    }

}
