package com.webroot.vo;

import java.io.Serializable;

/**
 *
 * @author ThirupathiReddy V
 *
 */
public class CreateLicenseRequest implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5268179665255290130L;

    private String requestid;

    private String oemid;

    private String deviceid;

    private String uid;

    private CreatePayload[] values;

    private String[] queries = new String[] { "createlicenseex" };

    private int xml = 0;

    public String getRequestid() {
        return requestid;
    }

    public void setRequestid(String requestid) {
        this.requestid = requestid;
    }

    public String getOemid() {
        return oemid;
    }

    public void setOemid(String oemid) {
        this.oemid = oemid;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public CreatePayload[] getValues() {
        return values;
    }

    public void setValues(CreatePayload[] values) {
        this.values = values;
    }

    public String[] getQueries() {
        return queries;
    }

    public void setQueries(String[] queries) {
        this.queries = queries;
    }

    public int getXml() {
        return xml;
    }

    public void setXml(int xml) {
        this.xml = xml;
    }

}
