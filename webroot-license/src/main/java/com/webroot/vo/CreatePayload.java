package com.webroot.vo;

import java.io.Serializable;

/**
 *
 * @author ThirupathiReddy V
 *
 */
public class CreatePayload implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1390590778757707742L;

    private String first_name;

    private String last_name;

    private String company_name;

    private String company_email;

    private String phone_number;

    private String country;

    private String license_request_source;

    public CreatePayload() {
    }

    /**
     *
     * @param first_name
     * @param last_name
     * @param company_name
     * @param company_email
     * @param phone_number
     * @param country
     * @param license_request_source
     */
    public CreatePayload(String first_name, String last_name, String company_name, String company_email, String phone_number, String country,
            String license_request_source) {
        super();
        this.first_name = first_name;
        this.last_name = last_name;
        this.company_name = company_name;
        this.company_email = company_email;
        this.phone_number = phone_number;
        this.country = country;
        this.license_request_source = license_request_source;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getCompany_email() {
        return company_email;
    }

    public void setCompany_email(String company_email) {
        this.company_email = company_email;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLicense_request_source() {
        return license_request_source;
    }

    public void setLicense_request_source(String license_request_source) {
        this.license_request_source = license_request_source;
    }

}
