package com.webroot.vo;

import java.io.Serializable;

/**
 *
 * @author ThirupathiReddy V
 *
 */
public class CheckLicense implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7275381375966671143L;

    private String oemid;

    private String deviceid;

    private String uid;

    public CheckLicense(String oemid, String deviceid, String uid) {
        super();
        this.oemid = oemid;
        this.deviceid = deviceid;
        this.uid = uid;
    }

    public String getOemid() {
        return oemid;
    }

    public void setOemid(String oemid) {
        this.oemid = oemid;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

}
