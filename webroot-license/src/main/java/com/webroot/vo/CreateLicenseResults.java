package com.webroot.vo;

import java.io.Serializable;

/**
 *
 * @author ThirupathiReddy V
 *
 */
public class CreateLicenseResults implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4968856190817657166L;

    private String value;

    private CreateLicenseQueries queries;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public CreateLicenseQueries getQueries() {
        return queries;
    }

    public void setQueries(CreateLicenseQueries queries) {
        this.queries = queries;
    }

}
