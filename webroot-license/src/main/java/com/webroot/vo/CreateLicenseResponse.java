package com.webroot.vo;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author ThirupathiReddy V
 *
 */
public class CreateLicenseResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2587343352162281839L;

    private int status;

    private String type;

    private List<CreateLicenseResults> results;

    private String status_description;

    private String error_code;

    private String error_code_description;

    private String errormsg;

    public String getStatus_description() {
        return status_description;
    }

    public void setStatus_description(String status_description) {
        this.status_description = status_description;
    }

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public String getError_code_description() {
        return error_code_description;
    }

    public void setError_code_description(String error_code_description) {
        this.error_code_description = error_code_description;
    }

    public String getErrormsg() {
        return errormsg;
    }

    public void setErrormsg(String errormsg) {
        this.errormsg = errormsg;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<CreateLicenseResults> getResults() {
        return results;
    }

    public void setResults(List<CreateLicenseResults> results) {
        this.results = results;
    }

}
