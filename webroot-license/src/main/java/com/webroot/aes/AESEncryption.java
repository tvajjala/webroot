package com.webroot.aes;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

/**
 * This class encrypts the generated UID for the brightCloud API. same instance is available with the same KEY <br>
 * in the adapter API to Decryption.
 *
 * @author ThirupathiReddy V
 *
 */
public class AESEncryption {

    // restricted access for security reason
    private static final String key = "BrightCloud12345";

    public static void main(String[] args) {

        try {
            final String text = "hparcsightes_41f8f131dfeb4c62953110f4425b2161";
            final String b = encrypt(text);
            System.out.println(b);
            System.out.println(decrypt(b));
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    public static String decrypt(String encoded) {

        try {
            final byte[] encrypted = Base64.decodeBase64(encoded);

            final Key aesKey = new SecretKeySpec(key.getBytes(), "AES");

            final Cipher cipher = Cipher.getInstance("AES");
            // decrypt the text
            cipher.init(Cipher.DECRYPT_MODE, aesKey);
            final String decrypted = new String(cipher.doFinal(encrypted));

            return decrypted;
        } catch (final Exception e) {
            System.out.println("Decryption " + e.getMessage());
        }

        return null;

    }

    public static String encrypt(String plainText) {
        try {
            final Key aesKey = new SecretKeySpec(key.getBytes(), "AES");

            final Cipher cipher = Cipher.getInstance("AES");
            // encrypt the text
            cipher.init(Cipher.ENCRYPT_MODE, aesKey);

            final byte[] encrypted = cipher.doFinal(plainText.getBytes());
            return Base64.encodeBase64String(encrypted);
        } catch (final Exception e) {
            System.out.println("Error while encryption " + e.getMessage());
        }
        return plainText;
    }
}
