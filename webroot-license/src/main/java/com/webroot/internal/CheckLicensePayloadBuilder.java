package com.webroot.internal;

import com.google.gson.Gson;
import com.webroot.vo.CheckLicense;
import com.webroot.vo.CheckLicenseRequest;

public class CheckLicensePayloadBuilder {

    private String deviceId;
    private String oemId;
    private String licenseUID;
    private String userUID;

    public static CheckLicensePayloadBuilder create() {
        return new CheckLicensePayloadBuilder();
    }

    public CheckLicensePayloadBuilder withDeviceId(String deviceId) {
        this.deviceId = deviceId;

        return this;
    }

    public CheckLicensePayloadBuilder oemId(String oemId) {
        this.oemId = oemId;
        return this;
    }

    public CheckLicensePayloadBuilder licenseUID(String licenseUID) {
        this.licenseUID = licenseUID;
        return this;
    }

    public CheckLicensePayloadBuilder userUID(String userUID) {
        this.userUID = userUID;
        return this;
    }

    public String asJson() {
        final CheckLicenseRequest checkLicenseRequest = new CheckLicenseRequest();
        checkLicenseRequest.setDeviceid(deviceId);
        checkLicenseRequest.setOemid(oemId);
        checkLicenseRequest.setUid(licenseUID);

        final CheckLicense[] values = { new CheckLicense(oemId, deviceId, userUID) };
        checkLicenseRequest.setValues(values);

        return new Gson().toJson(checkLicenseRequest);

    }

}
