package com.webroot.internal;

import com.google.gson.Gson;
import com.webroot.vo.CreateLicenseRequest;
import com.webroot.vo.CreatePayload;

public class CreateLicensePayloadBuilder {

    private String deviceId;
    private String oemId;
    private String licenseUID;

    private String fname;
    private String lname;
    private String company;
    private String email;
    private String phone;
    private String country;
    private String licenseRequestSource;

    public static CreateLicensePayloadBuilder create() {
        return new CreateLicensePayloadBuilder();
    }

    public CreateLicensePayloadBuilder withDeviceId(String deviceId) {
        this.deviceId = deviceId;

        return this;
    }

    public CreateLicensePayloadBuilder oemId(String oemId) {
        this.oemId = oemId;
        return this;
    }

    public CreateLicensePayloadBuilder licenseUID(String licenseUID) {
        this.licenseUID = licenseUID;
        return this;
    }

    public CreateLicensePayloadBuilder usingFirstname(String fname) {
        this.fname = fname;
        return this;
    }

    public CreateLicensePayloadBuilder lastname(String lname) {
        this.lname = lname;
        return this;
    }

    public CreateLicensePayloadBuilder email(String email) {
        this.email = email;
        return this;
    }

    public CreateLicensePayloadBuilder company(String company) {
        this.company = company;
        return this;
    }

    public CreateLicensePayloadBuilder country(String country) {
        this.country = country;
        return this;
    }

    public CreateLicensePayloadBuilder phone(String phone) {
        this.phone = phone;
        return this;
    }

    public CreateLicensePayloadBuilder usingLicenseRequestSource(String licenseRequestSource) {
        this.licenseRequestSource = licenseRequestSource;
        return this;
    }

    public String asJson() {

        final CreateLicenseRequest requestBody = new CreateLicenseRequest();
        requestBody.setDeviceid(deviceId);
        requestBody.setOemid(oemId);
        requestBody.setUid(licenseUID);

        final CreatePayload[] values = { new CreatePayload(fname, lname, company, email, phone, country, licenseRequestSource) };
        requestBody.setValues(values);

        return new Gson().toJson(requestBody);
    }

}
