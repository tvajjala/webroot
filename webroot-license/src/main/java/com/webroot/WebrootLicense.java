package com.webroot;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.regex.Pattern;

import com.google.gson.Gson;
import com.webroot.aes.AESEncryption;
import com.webroot.internal.CheckLicensePayloadBuilder;
import com.webroot.internal.CreateLicensePayloadBuilder;
import com.webroot.vo.CheckLicenseEx;
import com.webroot.vo.CheckLicenseResponse;
import com.webroot.vo.CreateLicenseEx;
import com.webroot.vo.CreateLicenseResponse;

/**
 * This class invoked from the installer on various scenarios.
 *
 * @author ThirupathiReddy V
 *
 */
public final class WebrootLicense {

    private static final String URL = "http://api.bcti.brightcloud.com/1.0/local";

    private static final String DEVICEID = "hp_arcsight";

    private static final String OEMID = "hp";

    private static final String LICENSE_UID = "hp_arcsight_connector";// is this always same value for license check

    private static String license_request_source = "hparcsightes";

    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    private static final String ALPHANUMERIC_PATTERN = "^[a-zA-Z0-9- ]*$";

    /** this file contains installation path which is auto populated when re-launch the installer */
    private static final String WEBROOT_FILE = ".webroot";

    public static void main(String[] args) {

        if (args.length >= 1) {
            final String category = args[0];
            if ("check".equalsIgnoreCase(category)) {
                if (args.length != 3) {
                    System.err.println("Please enter UID");
                } else {
                    if (args[1].trim().length() < 1) {
                        System.err.println("Please enter valid license");
                    } else {
                        checkLicense(args[1], args[2]);

                        saveInstallationPath(args[2]);

                    }
                }
            } else if ("create".equalsIgnoreCase(category)) {
                // System.out.println("hparcsightes_35bbdf7982954199a2fb4a46822c9c2f");

                if (args.length != 8) {
                    System.err.println("All fields are mandatory");
                } else {
                    final String fname = args[1];
                    if (fname == null || fname.trim().length() < 1) {
                        System.err.println("Please enter First Name");
                        return;
                    }

                    if (!Pattern.compile(ALPHANUMERIC_PATTERN).matcher(fname).matches()) {
                        System.err.println("The First Name you entered is invalid.\nPlease use only a-z, A-Z, 0-9, Spaces, Dash");
                        return;
                    }

                    final String lname = args[2];
                    if (lname == null || lname.trim().length() < 1) {
                        System.err.println("Please enter Last Name");
                        return;
                    }

                    if (!Pattern.compile(ALPHANUMERIC_PATTERN).matcher(lname).matches()) {
                        System.err.println("The Last Name you entered is invalid.\nPlease use only a-z, A-Z, 0-9, Spaces, Dash");
                        return;
                    }

                    final String company = args[3];
                    if (company == null || company.trim().length() < 1) {
                        System.err.println("Please enter Company");
                        return;
                    }
                    final String email = args[4];

                    if (email == null || email.trim().length() < 1) {
                        System.err.println("The Email you entered is invalid");
                        return;
                    }

                    if (!Pattern.compile(EMAIL_PATTERN).matcher(email).matches()) {
                        System.err.println("The Email you entered is invalid");
                        return;
                    }

                    final String phone = args[5];
                    if (phone == null || phone.trim().length() < 7) {
                        System.err.println("The Phone Number you entered is invalid");
                        return;
                    }

                    final String country = args[6];
                    if (country == null || country.trim().length() < 1) {
                        System.err.println("Please Select Country");
                        return;
                    }
                    final String installPath = args[7];
                    createLicense(fname, lname, company, email, phone, country, installPath);
                    saveInstallationPath(installPath);
                }
            } else if ("config".equalsIgnoreCase(category)) {
                downloadFrequency(args[1], args[2], args[4]);
                cefGenerationPath(args[3], args[4]);
                saveInstallationPath(args[4]);
            } else if ("getpath".equalsIgnoreCase(category)) {
                final String installationPath = getPreviousInstallationPath();
                if (installationPath != null) {
                    System.out.println(getPreviousInstallationPath());
                }
            } else if ("installPath".equalsIgnoreCase(category)) {
                final String installationPath = getPreviousInstallationPath();
                if (installationPath != null && isInstallationAlreadyExist(installationPath)) {
                    System.out.println(installationPath);
                } else {
                    // TODO: instead of hard-coding Webroot we can also pass vendor.name
                    final String path = args[1] + File.separator + "Webroot" + File.separator + args[2];
                    System.out.println(path);
                }
            } else if ("deleteDB".equalsIgnoreCase(category)) {
                deleteDB();
            } else if ("getOldIPConfig".equalsIgnoreCase(category)) {

                final String installPath = args[1];
                final Properties properties = checkAndCreateDefaultPropertiesFile(installPath);
                final String cronExpression = properties.getProperty("cron.expression");
                String cronKey = "1HOUR";

                if ("0 */30 * * * ?".equalsIgnoreCase(cronExpression)) {
                    cronKey = "15MIN";
                } else if ("0 0/60 * * * ?".equalsIgnoreCase(cronExpression)) {
                    cronKey = "1HOUR";
                } else if ("0 0 0/12 * * ?".equalsIgnoreCase(cronExpression)) {
                    cronKey = "12HOUR";
                } else if ("0 0 0/24 * * ?".equalsIgnoreCase(cronExpression)) {
                    cronKey = "24HOUR";
                }
                System.out.println(cronKey);

            } else {
                System.out.println("$/>webroot-license.jar  check UID");
                System.out.println("$/>webroot-license.jar  create fname lname company email phone country");
            }
        } else {
            System.out.println("$/>webroot-license.jar  check UID");
            System.out.println("$/>webroot-license.jar  create fname lname company email phone country");
        }

    }

    private static void downloadFrequency(String downloadType, String frequency, String installPath) {

        String cronExpression = "0 0/60 * * * ?";// set to default value
        frequency = frequency.trim();
        if ("15MIN".equalsIgnoreCase(frequency)) {
            cronExpression = "0 */30 * * * ?";// every 15 minutes
        } else if ("1HOUR".equalsIgnoreCase(frequency)) {
            cronExpression = "0 0/60 * * * ?";
        } else if ("12HOUR".equalsIgnoreCase(frequency)) {
            cronExpression = "0 0 0/12 * * ?";// 12 hours
        } else if ("24HOUR".equalsIgnoreCase(frequency)) {
            cronExpression = "0 0 0/24 * * ?";// 24 hours //TODO: check this
        }

        try {
            final Properties prop = checkAndCreateDefaultPropertiesFile(installPath);
            prop.load(new FileInputStream(installPath + File.separator + "connector.properties"));
            prop.put("cron.expression", cronExpression);
            prop.put("download.immediately", downloadType);
            prop.store(new FileOutputStream(installPath + File.separator + "connector.properties"),
                    "Webroot adapter configuration properties. Please don't edit this file");
        } catch (final Exception e) {
            System.err.println(e.getMessage() + " : " + installPath);
        }

    }

    private static void cefGenerationPath(String cefPath, String installPath) {

        try {

            if (!new File(cefPath).exists()) {
                System.out.println("Please enter valid path for CEF files generation");
                return;
            }

            final File cefFolder = new File(cefPath + File.separator + "CEF");

            if (!cefFolder.exists()) {
                cefFolder.mkdirs();
            }
            final Properties prop = checkAndCreateDefaultPropertiesFile(installPath);
            prop.load(new FileInputStream(installPath + File.separator + "connector.properties"));
            prop.put("cef.folder", cefFolder.getAbsolutePath());
            prop.store(new FileOutputStream(installPath + File.separator + "connector.properties"),
                    "Webroot adapter configuration properties. Please don't edit this file");
        } catch (final Exception e) {
            System.err.println("Error while updating CEF file path" + e.getMessage() + "> " + installPath);
        }

    }

    private static void checkLicense(String inputUID, String installPath) {
        try {

            final URL localURL = new URL(URL);
            final HttpURLConnection localHttpURLConnection = (HttpURLConnection) localURL.openConnection();
            localHttpURLConnection.setRequestMethod("POST");
            localHttpURLConnection.setRequestProperty("Content-Type", "application/json");

            final String checkLicensePayload = CheckLicensePayloadBuilder.create().withDeviceId(DEVICEID).oemId(OEMID).licenseUID(LICENSE_UID)
                    .userUID(inputUID).asJson();

            localHttpURLConnection.setDoOutput(true);
            final DataOutputStream localDataOutputStream = new DataOutputStream(localHttpURLConnection.getOutputStream());
            localDataOutputStream.writeBytes(checkLicensePayload);
            localDataOutputStream.flush();
            localDataOutputStream.close();

            if (localHttpURLConnection.getResponseCode() == 200) {
                final BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(localHttpURLConnection.getInputStream()));
                String line = null;
                final StringBuffer buffer = new StringBuffer();
                while ((line = localBufferedReader.readLine()) != null) {
                    buffer.append(line);
                }
                final String responseJson = buffer.toString();

                final CheckLicenseResponse checkLicenseResponse = new Gson().fromJson(responseJson, CheckLicenseResponse.class);

                if (checkLicenseResponse.getStatus() == 200) {

                    final CheckLicenseEx checkLicenseEx = checkLicenseResponse.getResults().get(0).getQueries().getChecklicenseex();

                    if (checkLicenseEx.getSuccess() == 1) {
                        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

                        final Date expiryDate = sdf.parse(checkLicenseEx.getExpiry_date());
                        if (expiryDate.after(new Date())) {// if it is future date
                            // license valid
                            System.out.println("License is valid ");
                            generatePropertiesFile(OEMID, DEVICEID, inputUID, installPath);
                        } else {
                            System.err.println("License expired. Please input valid license");
                        }

                    } else {
                        final String err = "Credential does not exist";
                        if (err.equalsIgnoreCase(checkLicenseEx.getErrormsg())) {
                            System.err.println("We are not able to verify your license. Please contact sales@webroot.com");
                        } else {
                            System.err.println(checkLicenseEx.getErrormsg());
                        }
                    }

                } else {
                    System.err.println(checkLicenseResponse.getError_code() + " : " + checkLicenseResponse.getErrormsg());
                }

            }

        } catch (final Exception e) {
            if (e instanceof UnknownHostException || e.getCause() instanceof UnknownHostException) {
                System.err.println("You need working internet connection to proceed.");
            } else {
                System.err.println("Error while connecting to server :" + e.getMessage());
            }
        }
    }

    /**
     * creating license
     */
    private static void createLicense(String fname, String lname, String company, String email, String phone, String country, String installPath) {
        try {
            final URL localURL = new URL(URL);

            final HttpURLConnection httpConnection = (HttpURLConnection) localURL.openConnection();
            httpConnection.setRequestMethod("POST");
            httpConnection.setRequestProperty("Content-Type", "application/json");

            final String reqPayload = CreateLicensePayloadBuilder.create().withDeviceId(DEVICEID).oemId(OEMID).licenseUID(LICENSE_UID).usingFirstname(fname)
                    .lastname(lname).company(company).email(email).phone(phone).country(country).usingLicenseRequestSource(license_request_source).asJson();

            httpConnection.setDoOutput(true);
            final DataOutputStream localDataOutputStream = new DataOutputStream(httpConnection.getOutputStream());
            localDataOutputStream.writeBytes(reqPayload);
            localDataOutputStream.flush();
            localDataOutputStream.close();

            if (httpConnection.getResponseCode() == 200) {
                final BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
                String line = null;
                final StringBuffer buffer = new StringBuffer();
                while ((line = localBufferedReader.readLine()) != null) {
                    buffer.append(line);
                }
                final String responseJson = buffer.toString();

                final CreateLicenseResponse licenseResponse = new Gson().fromJson(responseJson, CreateLicenseResponse.class);

                if (licenseResponse.getStatus() == 200) {

                    final CreateLicenseEx licenseEx = licenseResponse.getResults().get(0).getQueries().getCreatelicenseex();
                    if (licenseEx.getSuccess() == 1) {
                        generatePropertiesFile(licenseEx.getOemid(), licenseEx.getDeviceid(), licenseEx.getUid(), installPath);
                        System.out.println(licenseEx.getUid());// this needs to be displayed on installer
                    } else {

                        if (!"User already exists".equalsIgnoreCase(licenseEx.getErrormsg())) {
                            System.err.println(licenseEx.getErrormsg());
                        } else {
                            final Properties prop = checkAndCreateDefaultPropertiesFile(installPath);
                            final String uid = prop.getProperty("uid");
                            if (uid == null || uid.trim().length() < 1) {
                                System.err
                                        .println("A user with the same email address already exists. Please go back to the previous screen to enter the existing license associated with that email or register with a different email.");
                            } else {
                                System.out.println(AESEncryption.decrypt(prop.getProperty("uid")));
                            }
                        }

                    }
                } else {
                    System.err.println(licenseResponse.getError_code() + " :: " + licenseResponse.getErrormsg());
                }

            } else {
                System.err.println("Unable to process request. Http status :: " + httpConnection.getResponseCode());
            }

        } catch (final Exception ex) {
            if (ex instanceof UnknownHostException || ex.getCause() instanceof UnknownHostException) {
                System.err.println("You need working internet connection to proceed.");
            } else {
                System.err.println("Error while connecting to server :" + ex.getMessage());
            }

        }
    }

    private static void generatePropertiesFile(String oemid, String deviceid, String uid, String installPath) {

        try {
            final Properties properties = checkAndCreateDefaultPropertiesFile(installPath);
            // properties.put("oemid", oemid);
            // properties.put("deviceid", deviceid);
            properties.put("uid", AESEncryption.encrypt(uid));
            properties.store(new FileOutputStream(installPath + File.separator + "connector.properties"),
                    "Webroot adapter configuration properties. Please don't edit this file");
        } catch (final Exception e) {
        }

    }

    private static Properties checkAndCreateDefaultPropertiesFile(String installPath) {

        final File installFolder = new File(installPath);

        if (!installFolder.exists()) {
            installFolder.mkdirs();
        }

        final Properties properties = new Properties();
        final File propFile = new File(installPath + File.separator + "connector.properties");
        try {
            if (!propFile.exists()) {
                // create new file
                properties.store(new FileOutputStream(propFile), "Webroot adapter configuration properties. Please don't edit this file");
            } else {
                // load existing file
                properties.load(new FileInputStream(propFile));
            }

        } catch (final Exception e) {
            System.err.println(e.getMessage() + "(Error while creating properties file) " + installPath + File.separator);
        }

        return properties;
    }

    private static void saveInstallationPath(String installPath) {
        try {
            final String secretPath = System.getProperty("user.home");
            final File installLocation = new File(secretPath + File.separator + WEBROOT_FILE);
            final FileOutputStream fos = new FileOutputStream(installLocation);
            fos.write(installPath.getBytes());
            fos.close();
        } catch (final Exception e) {
        }
    }

    private static void deleteDB() {

        try {
            final String secretPath = System.getProperty("user.home");
            final File installLocation = new File(secretPath + File.separator + "webrootDB");
            if (installLocation.exists()) {
                installLocation.delete();
            }
        } catch (final Exception e) {

        }

    }

    private static String getPreviousInstallationPath() {

        String installationPath = null;
        try {
            final String secretPath = System.getProperty("user.home");
            final File installLocation = new File(secretPath + File.separator + WEBROOT_FILE);
            final FileInputStream fis = new FileInputStream(installLocation);

            final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fis));
            installationPath = bufferedReader.readLine();

            bufferedReader.close();

        } catch (final Exception e) {

        }

        return installationPath;

    }

    private static boolean isInstallationAlreadyExist(String installPath) {

        try {
            final String propertiesFile = installPath + File.separator + "connector.properties";
            final String jarFile = installPath + File.separator + "webroot-adapter.jar";
            return new File(propertiesFile).exists() && new File(jarFile).exists();
        } catch (final Exception e) {
        }

        return false;

    }

}
