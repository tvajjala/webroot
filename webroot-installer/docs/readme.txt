Readme for Webroot BrightCloud Threat Intelligence for HP ArcSight


Introduction
------------
The Webroot BrightCloud Threat Intelligence for HP ArcSight enables detection, alert, and investigation of malicious IP activities. It provides ArcSight customers with BrightCloud IP Reputation Service data to correlate with log files collected by ArcSight, detect malicious IP activities in incoming IP traffic, alert infosec teams, and provide them with detailed information on each malicious IP for incident response and investigation before those activities lead to security breaches. 


Use Cases
---------
- Detect & alert - Correlate with log data inside HP ArcSight to detect & alert on malicious IP activities so infosec team can perform incident response & investigation as early as possible before those activities lead to costly breaches
- Investigate - Provide detailed information on malicious IPs inside HP ArcSight for infosec teams to perform incident response & investigation


Key Features
------------
- Continuously downloads the most current 12M malicious IPs from BrightCloud IP Reputation Service to HP ArcSight

- Provides out-of-the-box dashboards to correlate BrightCloud IP threat intelligence data with log files and detect malicious IP activities in real-time

- Provides detailed information on each malicious IP on demand for incident response & investigation


Prerequisites
-------------
HP ArcSight V6.0 ESM Console and higher


Installation
------------
It is a simple 3 step process:
- Install Webroot BrightCloud for HP ArcSight ARB package for HP ArcSight ESM Console
- Install Webroot BrightCloud connector for HP ArcSight
- Install a HP ArcSight SmartConnector in the same server and configure it to pull CEF events from the BrightCloud connector for HP ArcSight

For full installation & user guide, please refer to this URL
http://download.webroot.com/Webroot_BrightCloud_For_HP_ArcSight.pdf


How to contact us
------------------------
sales@webroot.com for questions on licensing & sales
support@brightcloud.com for technical support