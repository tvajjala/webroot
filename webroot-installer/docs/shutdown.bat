:: Author : ThirupathiReddy Vajjala
:: Vendor : Webroot
:: Version: 1.0
:: Description: This batch file will stop BrightCloud connector for HP ArcSight java process.

@echo off

javaw -cp "#{INSTALLDIR}/webroot-adapter.jar" com.webroot.Shutdown