:: Author : ThirupathiReddy Vajjala
:: Vendor : Webroot
:: Version: 1.0
:: Description: This batch file will run BrightCloud connector for HP ArcSight java process in the background.

@echo off
 
:: CHANGE THE COMMAND SHELL DIRECTORY TO THE INSTALLATION DIRECTORY

pushd "#{INSTALLDIR}"

:: START THE JAVA PROCESS IN THE HEADLESS MODE

start javaw -DloggerLocation="#{INSTALLDIR}\log4j2.xml" -jar "webroot-adapter.jar" --spring.config.location=file:"#{INSTALLDIR}\connector.properties" &