#!/bin/bash
#  Author  :  ThirupathiReddy Vajjala
#  Product :  BrightCloud connector for HP ArcSight
#  Version :  1.0
#  Vendor  :  Webroot
#  Description : This shell script runs java process as a service in the linux machine.



# Below condition verifies user running as a super user or not
if [[ $EUID -ne 0 ]]; then
   echo "Please run this service as a super user " 
   echo "Example : $/>sudo service BrightCloudConnector stop"
   exit 1
fi


#Below code will terminates the running service

java -cp "#{INSTALLDIR}/webroot-adapter.jar" com.webroot.Shutdown