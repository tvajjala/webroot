#!/bin/bash
#  Author  :  ThirupathiReddy Vajjala
#  Product :  BrightCloud connector for HP ArcSight
#  Version :  1.0
#  Vendor  :  Webroot
#  Description : This shell script runs java process as a service in the linux machine.



# Below condition verifies user running as a super user or not
if [[ $EUID -ne 0 ]]; then
   echo "Please run this service as a super user " 
   echo "Example : $/>sudo service BrightCloudConnector start/stop"
   exit 1
fi

# Below is the actual code which executes to java process. 
# Place holder value will be replaced with actual path by the installer at the time of installation.

echo "###########################################################" 
echo "#########  BrightCloud connector for HP ArcSight  #########"
echo "###########################################################" 

if [ "$1" = "stop" ]
then
   echo "Stopping ..."
   
   java -cp "#{INSTALLDIR}/webroot-adapter.jar" com.webroot.Shutdown

   echo "BrightCloud connector for HP ArcSight Stopped Successfully."

elif [ "$1" = "start" ]
then
   echo "Starting..."
   
   java -cp "#{INSTALLDIR}/webroot-adapter.jar" com.webroot.Shutdown

   sleep 3s

   java -DloggerLocation="#{INSTALLDIR}/log4j2.xml" -jar "#{INSTALLDIR}/webroot-adapter.jar" --spring.config.location=file:"#{INSTALLDIR}/connector.properties" &

   echo "BrightCloud connector for HP ArcSight Started Successfully."

else
   echo "Starting..."

   java -cp "#{INSTALLDIR}/webroot-adapter.jar" com.webroot.Shutdown

   sleep 3s
   
   java -DloggerLocation="#{INSTALLDIR}/log4j2.xml" -jar "#{INSTALLDIR}/webroot-adapter.jar" --spring.config.location=file:"#{INSTALLDIR}/connector.properties" &

   echo "BrightCloud connector for HP ArcSight Started Successfully."

   echo "----------------------------------------------------------------------"
   echo "NOTE : You can control this service using below command"

   echo "$/>sudo service BrightCloudConnector stop/start"
   echo ""
   echo "Log level can be customized using below file."
   echo "#{INSTALLDIR}/log4j2.xml"
   echo "----------------------------------------------------------------------"
   echo ""
fi


