package com.webroot.job;

import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.webroot.filter.OldFileFilter;
import com.webroot.service.BrightCloudService;

/**
 * This scheduler deletes the processed files.
 *
 * @author ThirupathiReddy V
 *
 */
public class OldFileCleanUpJob extends QuartzJobBean {

    /** the logger */
    private static final Logger LOGGER = LogManager.getLogger(OldFileCleanUpJob.class);

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        try {

            final BrightCloudService brightCloudService = (BrightCloudService) context.getJobDetail().getJobDataMap().get("brightCloudService");

            final String cefPath = brightCloudService.getCEFGenerationPath();

            LOGGER.info("Cleaning up the CEF from the path: {} ", cefPath);

            final File cefFolder = new File(cefPath);

            final String[] oldFiles = cefFolder.list(new OldFileFilter("old"));
            for (final String oldFile : oldFiles) {
                deleteFile(cefPath + File.separator + oldFile);
            }
        } catch (final Exception e) {
            LOGGER.error("Error while running the cleanup scheduler", e);
        }

    }

    void deleteFile(String oldFile) {
        try {
            new File(oldFile).delete();
        } catch (final Exception e) {
            LOGGER.warn("Unable to delete the file", e);
        }

    }

    public static void main(String[] args) {
        final File cefFolder = new File("/Volumes/DATA/CEF");

        final String[] oldFiles = cefFolder.list(new OldFileFilter("cef"));

        for (final String ol : oldFiles) {

            new File("/Volumes/DATA/CEF" + File.separator + ol).delete();

        }
    }

}
