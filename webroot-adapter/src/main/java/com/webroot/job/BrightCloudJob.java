package com.webroot.job;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.webroot.service.BrightCloudService;

/**
 * This ScheduledJob starts the processing with a specified interval
 *
 * @author ThirupathiReddy V
 *
 */
public class BrightCloudJob extends QuartzJobBean {

    /** the logger */
    private static final Logger LOGGER = LogManager.getLogger(BrightCloudJob.class);

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        LOGGER.info("##################  SCHEDULER STARTED  ##################");
        final BrightCloudService brightCloudService = (BrightCloudService) context.getJobDetail().getJobDataMap().get("brightCloudService");

        brightCloudService.startScheduleProcessing();

    }

}
