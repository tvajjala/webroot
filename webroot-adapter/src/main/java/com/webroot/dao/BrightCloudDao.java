package com.webroot.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import com.webroot.constant.Constants;
import com.webroot.vo.IPData;
import com.webroot.vo.IPInformation;

/**
 * This interface exposes all the database operations on derby database
 *
 * @author ThirupathiReddy V
 *
 */
public interface BrightCloudDao {

    /**
     *
     * @param minorVersion
     * @return true if version changes
     */
    public boolean isVersionChanged(int minorVersion);

    /**
     *
     * @return true if we are running for the first time
     */
    public boolean isFirstTime();

    /**
     * process batch execution with given chunk size in {@link Constants}.
     *
     * @param query
     * @param ipDataList
     */
    public void processBatchInsert(final String query, List<IPData> ipDataList);

    /**
     *
     * @param ipUpdatesList
     */
    public void updateScoreFromCloud(List<IPData> ipUpdatesList);

    /**
     *
     * @param query
     * @return number of records
     */
    public int getCount(final String query);

    /**
     *
     * @param query
     * @param offset
     * @return as list of beans
     */
    public List<IPData> getIPRecords(final String query, int offset);

    /**
     *
     * @return true, if records already exists in the latest table
     */
    public boolean isRecordsExistsInTheLatestTable();

    /**
     * move the existing data to old table before downloading new version
     */
    public void copyDataToTheOldTable();

    /**
     * cleans the master table data
     */
    public void cleanMasterTable();

    /**
     *
     * @return safeIPs
     */
    public Set<Long> getSafeIpsList();

    /**
     *
     * @return newThreatIPs
     */
    public Set<Long> getNewThreatIpList();

    /**
     *
     * @return categoryChangeIPs
     */
    public Set<Long> getCategoryChangeIpList();

    /**
     * generates CEF based on givens lists
     *
     * @param safeIps
     * @param newThreatIps
     * @param catChangeIps
     */
    public void generateCEFRecordsOnVersionChange(Set<Long> safeIps, Set<Long> newThreatIps, Set<Long> catChangeIps);

    /**
     * first time CEF generation from the OLD table
     */
    public void generateCEFsOnStartup();

    /**
     * update version on successful processing base file
     *
     * @param minorVersion
     */
    public void updateAPIVersionProcessed(int minorVersion);

    /**
     *
     * @param minorVersion
     * @return last processed order
     */
    public int getLastProcessedOrderNumber(int minorVersion);

    /**
     * update the currently processed order number
     *
     * @param minorVersion
     * @param orderNumber
     */
    public void updateCurrentlyProcessedOrderNumber(int minorVersion, int orderNumber);

    /**
     *
     * @param order
     */
    public void generateCEFsFromIpUpdates(int order);

    /**
     *
     * @param query
     * @throws Exception
     */
    public void executeSQLQuery(String query) throws SQLException;

    /**
     * remove all the records from IP_Updates table
     */
    public void cleanIpUpdates();

    /**
     *
     * @return total count for this update processing cycles
     */
    public int getIpUpdatesCount();

    /**
     * add/update new IPs that are available as part ip_updates to master table with action= '+'
     */
    public void addNewIPsToMasterTable();

    /**
     * remove IPs that indicated as '-' in updates from master table
     */
    public void removeIPsFromMasterTable();

    /**
     *
     * @param ipAddress
     * @return IPInformation form database
     */
    public IPInformation getFullIPInfoFromDB(String ipAddress);

    /**
     * saving ipInformation
     *
     * @param ipInformation
     */
    public void saveIPInformation(IPInformation ipInformation);

    /**
     * deleting geoInfo
     *
     * @param ipAddress
     */
    public void deleteGeoInfoRecord(String ipAddress);

    /**
     * processBulkDeletion
     *
     * @param ipDataList
     */
    public void processBulkDeletionOnMaster(List<IPData> ipDataList);

    /**
     *
     * @return license expiryDate
     */
    public String getLicenseExpiryMessage();

    /**
     * update processing state to false
     */
    public void updateProcessingFlagToFlase();

    /**
     * update processing state to true
     */
    public void updateProcessingFlagToTrue();

    /**
     *
     * @return state of the processingStarted
     */
    public boolean isProcessingStarted();

}
