package com.webroot.dao.impl;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.StrSubstitutor;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Iterables;
import com.webroot.aes.AESEncryption;
import com.webroot.constant.Constants;
import com.webroot.dao.BrightCloudDao;
import com.webroot.util.BCUtil;
import com.webroot.util.CIDRUtil;
import com.webroot.vo.CheckLicense;
import com.webroot.vo.CheckLicenseEx;
import com.webroot.vo.CheckLicenseRequest;
import com.webroot.vo.CheckLicenseResponse;
import com.webroot.vo.IPData;
import com.webroot.vo.IPInformation;
import com.webroot.vo.RepCountRequest;
import com.webroot.vo.RepResponse;
import com.webroot.vo.RepResult;

/**
 *
 * @author ThirupathiReddy V
 *
 */
public class BrightCloudDaoImpl implements BrightCloudDao, Constants {

    /** logger instance */
    private static final Logger LOGGER = LogManager.getLogger(BrightCloudDao.class);

    @Autowired
    private DataSource dataSource;

    @Autowired
    private Environment env;

    @Autowired
    private ObjectMapper objectMapper;

    private final ConcurrentMap<String, Boolean> processCheck = new ConcurrentHashMap<String, Boolean>();

    @PostConstruct
    public void init() {

        LOGGER.info("**************   DATA INITIALIZATION  *****************");

        try {
            executeSQLQuery(CREATE_THREAT_IP_MASTER);
        } catch (final Exception e) {
            LOGGER.trace("Error while executing query CREATE_THREAT_IP_MASTER", e);
        }

        try {
            executeSQLQuery(CREATE_THREAT_IP_SLAVE);
        } catch (final Exception e) {
            LOGGER.trace("Error while executing query CREATE_THREAT_IP_SLAVE", e);
        }

        try {
            executeSQLQuery(CREATE_IP_UPDATES);
        } catch (final Exception e) {
            LOGGER.trace("Error while executing query CREATE_IP_UPDATES", e);
        }

        try {
            executeSQLQuery(CREATE_VERSION_TBL);
        } catch (final Exception e) {
            LOGGER.trace("Error while executing query CREATE_VERSION_TBL", e);
        }

        try {
            executeSQLQuery(CREATE_ORDER_STATUS);
        } catch (final Exception e) {
            LOGGER.trace("Error while executing query CREATE_ORDER_STATUS", e);
        }

        try {
            executeSQLQuery(CREATE_IP_INFO_TBL);
        } catch (final Exception e) {
            LOGGER.trace("Error while executing query CREATE_IP_INFO_TBL", e);
        }

        try {
            executeSQLQuery(CREATE_PROCESSING_FLAG);
            executeSQLQuery(INSERT_PROCESS_TBL);
        } catch (final Exception e) {
            LOGGER.trace("Error while executing query CREATE_PROCESSING_FLAG and INSERT_PROCESS_TBL", e);
        }

        try {
            final File cefFolder = new File(env.getProperty(CEF_FOLDER));
            LOGGER.info("Genering CEF files at :: {} ", cefFolder.getAbsolutePath());
            if (!cefFolder.exists()) {
                cefFolder.mkdirs();
            }
        } catch (final Exception e) {
            LOGGER.trace("Error while creating folders ", e);
        }

    }

    @Override
    public IPInformation getFullIPInfoFromDB(String ipAddress) {
        if (ipAddress == null) {
            return null;
        }
        final String ipParam = ipAddress;
        IPInformation ipInformation = null;

        try {
            final Connection connection = dataSource.getConnection();
            final PreparedStatement ps = connection.prepareStatement(SELECT_IP_TBL);
            ps.setString(1, ipParam);
            final ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                ipInformation = new IPInformation();
                ipInformation.setAsn(rs.getString(1));
                ipInformation.setCarrier(rs.getString(2));
                ipInformation.setCity(rs.getString(3));
                ipInformation.setCountry(rs.getString(4));
                ipInformation.setCurrent_release_date(rs.getLong(5));
                ipInformation.setDomain(rs.getString(6));
                ipInformation.setDomain_age(rs.getString(7));
                ipInformation.setFirst_release_date(rs.getLong(8));
                ipInformation.setIp_status(rs.getInt(9));
                ipInformation.setIpAddress(rs.getString(10));
                ipInformation.setIpint(rs.getLong(11));
                ipInformation.setLast_release_date(rs.getLong(12));
                ipInformation.setLastupdated(rs.getLong(13));
                ipInformation.setLatitude(rs.getString(14));
                ipInformation.setLongitude(rs.getString(15));
                ipInformation.setOrganization(rs.getString(16));
                ipInformation.setRegion(rs.getString(17));
                ipInformation.setReputation(rs.getInt(18));
                ipInformation.setSld(rs.getString(19));
                ipInformation.setState(rs.getString(20));
                ipInformation.setThreat_count(rs.getInt(21));
                ipInformation.setThreat_mask(rs.getInt(22));
                ipInformation.setTld(rs.getString(23));
            }
            rs.close();
            ps.close();
            connection.close();
        } catch (final Exception e) {
            LOGGER.error("Unable to read data from IP_INFO table ", e);
        }
        return ipInformation;
    }

    @Override
    public void saveIPInformation(IPInformation ipInformation) {

        try {
            final Connection connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            final PreparedStatement ps = connection.prepareStatement(INSERT_IP_TBL);

            ps.setString(1, ipInformation.getAsn());
            ps.setString(2, ipInformation.getCarrier());
            ps.setString(3, ipInformation.getCity());
            ps.setString(4, ipInformation.getCountry());
            ps.setLong(5, ipInformation.getCurrent_release_date());
            ps.setString(6, ipInformation.getDomain());
            ps.setString(7, ipInformation.getDomain_age());
            ps.setLong(8, ipInformation.getFirst_release_date());
            ps.setInt(9, ipInformation.getIp_status());
            ps.setString(10, ipInformation.getIpAddress());
            ps.setLong(11, ipInformation.getIpint());
            ps.setLong(12, ipInformation.getLast_release_date());
            ps.setLong(13, System.currentTimeMillis());
            ps.setString(14, ipInformation.getLatitude());
            ps.setString(15, ipInformation.getLongitude());
            ps.setString(16, ipInformation.getOrganization());
            ps.setString(17, ipInformation.getRegion());
            ps.setInt(18, ipInformation.getReputation());
            ps.setString(19, ipInformation.getSld());
            ps.setString(20, ipInformation.getState());
            ps.setInt(21, ipInformation.getThreat_count());
            ps.setInt(22, ipInformation.getThreat_mask());
            ps.setString(23, ipInformation.getTld());
            ps.execute();
            connection.commit();
            ps.close();
            connection.close();
        } catch (final Exception e) {
            LOGGER.error("error while inserting geoInformation", e);
        }
    }

    @Override
    public void deleteGeoInfoRecord(String ipAddress) {

        try {
            final Connection connection = dataSource.getConnection();
            final PreparedStatement ps = connection.prepareStatement(DELETE_IP_TBL);
            ps.setString(1, ipAddress);
            ps.executeUpdate();
            ps.close();
            connection.close();
        } catch (final Exception e) {
            LOGGER.error("error while deleting the geoInformation", e);
        }

    }

    @Override
    public boolean isVersionChanged(int minorVersion) {

        try {
            final Integer dbMinorVersion = getCount(SELECT_VERSIONT_TBL);
            LOGGER.info("Last processed  minorVersion  :: {} ", dbMinorVersion);
            LOGGER.info("New minorVersion received :: {} ", minorVersion);
            return dbMinorVersion != minorVersion;

        } catch (final Exception e) {
            LOGGER.debug("Error while checking version change", e);
            LOGGER.error("Error while checking the version change {} ", e.getMessage());
        }
        // default return true ,so that it will start processing base files
        return true;

    }

    @Override
    public boolean isFirstTime() {
        final int oldDataCount = getCount(VERION_COUNT);
        return oldDataCount == 0;
    }

    @Override
    public void processBatchInsert(final String query, List<IPData> ipDataList) {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            final PreparedStatement stmt = connection.prepareStatement(query);
            for (final IPData ipData : ipDataList) {
                stmt.setLong(1, ipData.getDecimalIp());
                stmt.setInt(2, ipData.getRange());
                stmt.setInt(3, ipData.getThreatType());
                stmt.setInt(4, ipData.getRepCount());
                stmt.addBatch();
            }
            stmt.executeBatch();
            stmt.close();
            connection.commit();
        } catch (final Exception e) {
            try {
                connection.rollback();
            } catch (final SQLException e1) {
                LOGGER.debug("Rollback error ", e1);
            }
            LOGGER.warn("Error while inserting into db", e);
            LOGGER.error("Error while inserting into db {} ", e.getMessage());
        } finally {

            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (final Exception e) {
                LOGGER.debug(e);
            }
        }

    }

    @Override
    public void updateScoreFromCloud(List<IPData> ipUpdatesList) {

        Connection connection = null;
        // Map contains key as DecimalIP and Value as repScore from cloud
        final Map<Long, Integer> mapRep = new ConcurrentHashMap<Long, Integer>(ipUpdatesList.size());

        // Assuming that single update file decimal IPs are not duplicate
        final Set<Long> uniqueKeys = new HashSet<Long>(ipUpdatesList.size());

        for (final IPData data : ipUpdatesList) {
            uniqueKeys.add(data.getDecimalIp());
        }

        int iteration = 0;
        do {
            updateRepCountAsync(mapRep, uniqueKeys);
            LOGGER.info("Received repCount for the IPS {} out of {} ", mapRep.size(), uniqueKeys.size());
            uniqueKeys.removeAll(mapRep.keySet());

            if (!uniqueKeys.isEmpty()) {
                try {
                    LOGGER.info("Moving into idle state (30 seconds) before next re-attempt");
                    Thread.sleep(30000);
                    LOGGER.info("Re-attempting with the keys size {} and attempt number {} ", uniqueKeys.size(), iteration);
                } catch (final InterruptedException e) {
                    LOGGER.debug("Sleeping time interrupted ", e);
                }
            }

            iteration++;

        } while (!uniqueKeys.isEmpty() && iteration < 3);

        if (!uniqueKeys.isEmpty()) {
            LOGGER.info("There  are {} IPs which doesn't have reputation count updated. adding default value=10", uniqueKeys.size());
        }

        LOGGER.info("Inserting ip_updates into database of size :{} ", mapRep.size());

        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            final PreparedStatement ps = connection.prepareStatement(INSERT_IP_UPDATE_TBL);
            for (final IPData ipData : ipUpdatesList) {
                final int repCount = mapRep.get(ipData.getDecimalIp()) == null ? 10 : mapRep.get(ipData.getDecimalIp());
                ps.setLong(1, ipData.getDecimalIp());
                ps.setInt(2, ipData.getRange());
                ps.setInt(3, ipData.getThreatType());
                ps.setInt(4, repCount);
                ps.setString(5, ipData.getAction());
                ps.addBatch();
            }
            ps.executeBatch();
            ps.close();
            connection.commit();
        } catch (final Exception e) {
            try {
                connection.rollback();
            } catch (final SQLException e1) {
                LOGGER.debug("Error while rollback", e1);
            }

            LOGGER.error("Error while inserting into ip_updates table ", e);
        } finally {
            try {
                uniqueKeys.clear();
                mapRep.clear();
                if (connection != null) {
                    connection.close();
                }
            } catch (final Exception e) {
                LOGGER.debug("Error while closing the connection", e);
            }
        }

    }

    @Override
    public void executeSQLQuery(String query) throws SQLException {

        final Connection connection = dataSource.getConnection();

        try {
            final Statement stmt = connection.createStatement();
            stmt.execute(query);
            stmt.close();
        } finally {
            connection.close();
        }
    }

    @Override
    public int getCount(final String query) {

        int count = -1;

        try {
            final Connection connection = dataSource.getConnection();
            final Statement stmt = connection.createStatement();
            final ResultSet result = stmt.executeQuery(query);

            if (result.next()) {
                count = result.getInt(1);
            }
            result.close();
            stmt.close();
            connection.close();
        } catch (final SQLException sqlException) {
            LOGGER.error("Error while executing the count query", sqlException);
        }

        return count;
        //
    }

    private Set<Long> getDecimalIPs(String query) {
        final Set<Long> dataList = new HashSet<Long>(1000);

        try {
            final Connection connection = dataSource.getConnection();

            final Statement stmt = connection.createStatement();
            final ResultSet result = stmt.executeQuery(query);

            while (result.next()) {
                dataList.add(result.getLong(1));
            }
            result.close();
            stmt.close();
            connection.close();
        } catch (final SQLException e) {
            LOGGER.error("Error while reading decimalIPs ", e);
        }

        return dataList;
    }

    @Override
    public boolean isRecordsExistsInTheLatestTable() {
        return getCount(THREAT_IP_MASTER_COUNT) > 0;
    }

    @Override
    public Set<Long> getSafeIpsList() {

        final Set<Long> safeIpSet = new HashSet<Long>(1000);
        int maxCount = getCount(THREAT_IP_SLAVE_COUNT);
        LOGGER.info("Total records in the slave table :: {}", maxCount);

        final Map<String, Integer> params = new HashMap<String, Integer>(1);
        int offset = 0;

        while (offset < maxCount) {
            params.put(OFFSET, offset);
            final String oldDBQuery = StrSubstitutor.replace(SELECT_IP_FROM_SLAVE_DB, params);
            safeIpSet.addAll(getDecimalIPs(oldDBQuery));
            offset += CHUNK_SIZE;
        }

        LOGGER.info("Total IP records received from slave table ::{} ", safeIpSet.size());

        // //////////////////////////////////////////////////////////////////////////////////////////////////
        // Removing duplicates from the new table

        offset = 0;
        maxCount = getCount(THREAT_IP_MASTER_COUNT);
        LOGGER.info("Total records found in the master table  ::{} ", maxCount);

        int totalProcessedCount = 0;
        while (offset < maxCount) {
            params.put(OFFSET, offset);
            final String newDbQuery = StrSubstitutor.replace(SELECT_IP_FROM_MASTER_DB, params);
            final Set<Long> tempList = getDecimalIPs(newDbQuery);
            totalProcessedCount += tempList.size();
            safeIpSet.removeAll(tempList);
            offset += CHUNK_SIZE;
        }
        LOGGER.info("Total processed records {}. Is all the records processed? {} ", totalProcessedCount, totalProcessedCount == maxCount);
        // //////////////////////////////////////////////////////////////////////////////////////////////////

        LOGGER.info("Returning filtered safeIps :: {} ", safeIpSet.size());

        return safeIpSet;
    }

    @Override
    public Set<Long> getNewThreatIpList() {

        final Set<Long> newThreatIpList = new HashSet<Long>(1000);

        int maxCount = getCount(THREAT_IP_MASTER_COUNT);
        LOGGER.info("Total records in the latest table :: {} ", maxCount);

        final Map<String, Integer> params = new HashMap<String, Integer>(1);

        int offset = 0;

        while (offset < maxCount) {
            params.put(OFFSET, offset);
            final String newDbQuery = StrSubstitutor.replace(SELECT_IP_FROM_MASTER_DB, params);
            newThreatIpList.addAll(getDecimalIPs(newDbQuery));
            offset += CHUNK_SIZE;
        }

        LOGGER.info("Total IP records received from latest table ::{} ", newThreatIpList.size());

        // //////////////////////////////////////////////////////////////////////////////////////////////////
        // Removing duplicates from the new table
        offset = 0;
        maxCount = getCount(THREAT_IP_SLAVE_COUNT);

        LOGGER.info("Total records found in the slave table  :: {} ", maxCount);
        int totalProcessedCount = 0;
        while (offset < maxCount) {
            params.put(OFFSET, offset);
            final String oldDBQuery = StrSubstitutor.replace(SELECT_IP_FROM_SLAVE_DB, params);
            final Set<Long> tempSet = getDecimalIPs(oldDBQuery);
            totalProcessedCount += tempSet.size();
            newThreatIpList.removeAll(tempSet);
            offset += CHUNK_SIZE;
        }

        LOGGER.info("Total processed records  {}. Is all the records processed? {} ", totalProcessedCount, totalProcessedCount == maxCount);

        LOGGER.info("Returning filtered newThreaIps " + newThreatIpList.size());

        return newThreatIpList;

    }

    @Override
    public Set<Long> getCategoryChangeIpList() {

        final Set<Long> categoryChangeIPSet = new HashSet<Long>(1000);

        try {
            final Connection connection = dataSource.getConnection();

            final Statement stmt = connection.createStatement();

            final ResultSet result = stmt.executeQuery(SELECT_CHANGE_CATEGORY);

            while (result.next()) {
                categoryChangeIPSet.add(result.getLong(1));
            }
            result.close();
            stmt.close();
            connection.close();
        } catch (final Exception e) {
            LOGGER.error("Unable to execute the change in category retrieval query :: ", e);
        }

        LOGGER.info("Returning filtered change in category IPs " + categoryChangeIPSet.size());
        return categoryChangeIPSet;
    }

    public List<Long> getUpdateIpChunk(final String query, int offset) {
        LOGGER.info("Reading decimalIps from the ip_updates table ");
        final List<Long> dataList = new ArrayList<Long>();
        Connection connection = null;
        try {
            connection = dataSource.getConnection();

            final Statement stmt = connection.createStatement();

            final Map<String, Integer> params = new HashMap<String, Integer>(1);
            params.put(OFFSET, offset);

            final String searchQuery = StrSubstitutor.replace(query, params);

            final ResultSet result = stmt.executeQuery(searchQuery);

            while (result.next()) {
                dataList.add(result.getLong(1));
            }
            result.close();
            stmt.close();
            connection.close();
        } catch (final Exception e) {
            LOGGER.error("Error while reading Update IP chunk ", e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (final Exception e) {
                LOGGER.debug(e);
            }
        }
        return dataList;
    }

    @Override
    public List<IPData> getIPRecords(final String query, int offset) {
        final List<IPData> dataList = new ArrayList<IPData>();

        try {
            final Connection connection = dataSource.getConnection();

            final Statement stmt = connection.createStatement();

            final Map<String, Integer> params = new HashMap<String, Integer>(1);
            params.put(OFFSET, offset);

            final String searchQuery = StrSubstitutor.replace(query, params);

            final ResultSet result = stmt.executeQuery(searchQuery);

            while (result.next()) {
                dataList.add(new IPData(result.getLong(1), result.getInt(2), result.getInt(3), result.getInt(4)));
            }
            result.close();
            stmt.close();
            connection.close();
        } catch (final Exception e) {
            LOGGER.error("Error while reading IPRecords ", e);
        }

        return dataList;
    }

    public List<IPData> getIPUpdatesRecords(final String query, int offset) {
        final List<IPData> dataList = new ArrayList<IPData>();

        try {
            final Connection connection = dataSource.getConnection();

            final Statement stmt = connection.createStatement();

            final Map<String, Integer> params = new HashMap<String, Integer>(1);
            params.put(OFFSET, offset);

            final String searchQuery = StrSubstitutor.replace(query, params);

            final ResultSet result = stmt.executeQuery(searchQuery);

            while (result.next()) {
                dataList.add(new IPData(result.getLong(1), result.getInt(2), result.getInt(3), result.getInt(4), result.getString(5)));
            }
            result.close();
            stmt.close();
            connection.close();
        } catch (final Exception e) {
            LOGGER.error("Error while reading IPUpdates records ", e);
        }

        return dataList;
    }

    public void cleanSlaveTable() {
        try {
            LOGGER.info("Cleaning the slave table data ");
            executeSQLQuery(CLEAN_SLAVE_TABLE);
        } catch (final Exception e) {
            LOGGER.error("Exception while cleaning the slave table data  :  ", e);
        }
    }

    @Override
    public void copyDataToTheOldTable() {

        cleanSlaveTable();

        try {
            LOGGER.info("Copying data form master table to slave table ");
            executeSQLQuery(COPY_MASTER_SLAVE);
        } catch (final Exception e) {
            LOGGER.error("Exception while copying data  :  ", e);
        }

        cleanMasterTable();

    }

    @Override
    public void cleanMasterTable() {
        try {
            LOGGER.info("Cleaning the master table data ");
            executeSQLQuery(CLEAN_MASTER_TABLE);
        } catch (final Exception e) {
            LOGGER.error("Exception while cleaning the master data  :  ", e);
        }
    }

    @Override
    public void generateCEFRecordsOnVersionChange(Set<Long> safeIps, Set<Long> newThreatIps, Set<Long> catChangeIps) {

        LOGGER.info("Generating CEF files @Path :: " + env.getProperty(CEF_FOLDER));

        LOGGER.info("Total CEF events count ::  " + (safeIps.size() + newThreatIps.size() + catChangeIps.size()));

        final List<String> cefRecords = new ArrayList<String>(10000);

        for (final List<Long> subList : Iterables.partition(safeIps, 50)) {
            generateCEFRecordsFromSafetySubList(subList, cefRecords);
        }

        for (final List<Long> subList : Iterables.partition(newThreatIps, 50)) {
            generateCEFRecordsFromNewThreatSubList(subList, cefRecords);
        }

        for (final List<Long> subList : Iterables.partition(catChangeIps, 50)) {
            generateCEFRecordsFromNewCatChangeSubList(subList, cefRecords);
        }

        try {
            if (!cefRecords.isEmpty()) {
                final String fileName = env.getProperty(CEF_FOLDER) + File.separator + UUID.randomUUID().toString() + ".cef";
                final FileWriter writer = new FileWriter(fileName);
                for (final String event : cefRecords) {
                    writer.write(event + "\n");
                }
                writer.close();
                cefRecords.clear();
            }
        } catch (final Exception e) {
            LOGGER.error("Error while generating  CEF file ", e);
        }

    }

    @Override
    public void generateCEFsOnStartup() {
        final int maxCount = getCount(THREAT_IP_MASTER_COUNT);

        LOGGER.info("Generating CEF files @Path :: {} ", env.getProperty(CEF_FOLDER));

        int count = 0;
        int cefEventsCount = 0;

        final List<String> cefRecords = new ArrayList<String>(10000);

        while (count <= maxCount) {
            final List<IPData> ipData = getIPRecords(SELECT_MASTER_DB, count);

            for (final IPData ip : ipData) {

                final String[] addressList = CIDRUtil.getAddressList(ip.getDecimalIp(), ip.getRange());

                for (final String ipAddress : addressList) {

                    for (final String category : BCUtil.getCategoryList(ip.getThreatType())) {

                        cefRecords.add(BCUtil.getCEFFormat(ipAddress, category, ip.getRepCount(), CEF_VERSION, "add"));
                        cefEventsCount++;

                        if (cefRecords.size() == CEF_EVENT_COUNT) {
                            try {
                                final String fileName = env.getProperty(CEF_FOLDER) + File.separator + UUID.randomUUID().toString() + ".cef";
                                final FileWriter writer = new FileWriter(fileName);
                                for (final String line : cefRecords) {
                                    writer.write(line + "\n");
                                }
                                writer.close();
                                cefRecords.clear();
                            } catch (final Exception e) {
                                LOGGER.error("Error while generating CEF record ", e);
                            }
                        }

                    }
                }

            }
            count += CHUNK_SIZE;
        }

        if (!cefRecords.isEmpty()) {
            try {
                final String fileName = env.getProperty(CEF_FOLDER) + File.separator + UUID.randomUUID().toString() + ".cef";
                final FileWriter writer = new FileWriter(fileName);
                for (final String line : cefRecords) {
                    writer.write(line + "\n");
                }
                writer.close();
                cefRecords.clear();
            } catch (final Exception e) {
                LOGGER.error("Error while generating CEF records ", e);
            }
        }

        LOGGER.info("Basefile CEF generation completed with total CEF events: {} ", cefEventsCount);

    }

    @Override
    public void generateCEFsFromIpUpdates(int order) {

        final int maxCount = getCount(SELECT_IP_UPDATES_COUNT);

        if (maxCount == 0) {
            LOGGER.info("There are no new events found hence not generating any CEF events");
            return;
        }

        LOGGER.info("Generating CEF with the ip_updates: {} ", maxCount);

        int count = 0;

        final List<String> cefRecords = new ArrayList<String>(10000);

        while (count <= maxCount) {
            final List<IPData> ipDataList = getIPUpdatesRecords(SELECT_ALL_IP_UPDATES_TBL, count);

            for (final IPData ipData : ipDataList) {
                prepareCEFRecords(cefRecords, ipData, order);
            }

            count += CHUNK_SIZE;
        }

        if (!cefRecords.isEmpty()) {
            generateCEFRecord(cefRecords, order);
        }

    }

    void prepareCEFRecords(List<String> cefRecords, IPData ipData, int order) {

        String action = null;

        if ("+".equalsIgnoreCase(ipData.getAction())) {
            action = "add";
        } else if ("-".equalsIgnoreCase(ipData.getAction())) {
            action = "remove";
        }

        final String[] addressList = CIDRUtil.getAddressList(ipData.getDecimalIp(), ipData.getRange());

        for (final String ipAddress : addressList) {

            for (final String category : BCUtil.getCategoryList(ipData.getThreatType())) {

                cefRecords.add(BCUtil.getCEFFormat(ipAddress, category, ipData.getRepCount(), CEF_VERSION, action));
                if (cefRecords.size() == CEF_EVENT_COUNT) {
                    generateCEFRecord(cefRecords, order);
                }
            }
        }
    }

    void generateCEFRecord(List<String> cefRecords, int order) {

        try {

            final String fileName = env.getProperty(CEF_FOLDER) + File.separator + "update_" + order + "_" + UUID.randomUUID().toString() + ".cef";
            final FileWriter writer = new FileWriter(fileName);
            for (final String line : cefRecords) {
                writer.write(line + "\n");
            }
            writer.close();

            cefRecords.clear();
        } catch (final Exception e) {
            LOGGER.error("Error while generation CEF records ", e);
        }
    }

    void updateRepCountAsync(Map<Long, Integer> repCountMap, Set<Long> ipData) {

        if (!ipData.isEmpty()) {

            final Iterable<List<Long>> partLists = Iterables.partition(ipData, 100);

            int latchCount = 0;

            for (final List<Long> subList : partLists) {

                if (LOGGER.isTraceEnabled()) {
                    LOGGER.trace("subList decimal IPs " + subList);
                }

                latchCount++;
            }

            final ExecutorService executorService = Executors.newFixedThreadPool(50);
            LOGGER.info("Reading reputationScope from the cloud for the ip count :: {} ", ipData.size());
            final CountDownLatch countDownLatch = new CountDownLatch(latchCount);

            for (final List<Long> subList : partLists) {
                executorService.submit(new RepCountAsync(subList, repCountMap, countDownLatch));
            }

            try {
                countDownLatch.await();
            } catch (final InterruptedException e) {
                LOGGER.error("Countdown latch interrupted ", e);
            }

        }

    }

    class RepCountAsync implements Runnable {

        private final List<Long> list;

        private final Map<Long, Integer> repMap;
        CountDownLatch countDownLatch;

        RepCountAsync(List<Long> list, Map<Long, Integer> repMap, CountDownLatch countDownLatch) {
            this.list = list;
            this.repMap = repMap;
            this.countDownLatch = countDownLatch;
        }

        @Override
        public void run() {
            repMap.putAll(getReputationCountMap(list, countDownLatch));
        }
    }

    public void updateReputationCountInDB(Map<Long, Integer> repCountMap) {
        LOGGER.info("Updating reputation count value in ip_updates table with batch size :: {} ", repCountMap.size());
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            final PreparedStatement ps = connection.prepareStatement("UPDATE IP_UPDATES SET REP_COUNT=? WHERE DECIMAL_IP=?");
            for (final Entry<Long, Integer> entry : repCountMap.entrySet()) {
                ps.setInt(1, entry.getValue());
                ps.setLong(2, entry.getKey());
                ps.addBatch();
            }
            ps.executeBatch();
            ps.close();
            connection.commit();
        } catch (final Exception e) {
            try {
                connection.rollback();
            } catch (final SQLException e1) {
                LOGGER.debug("Connection rollback error", e1);
            }
            LOGGER.error("Error while updating repCount in ip_updates table ", e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (final Exception e) {
                LOGGER.debug(e);
            }
        }

    }

    Map<Long, Integer> getReputationCountMap(List<Long> decimalIp, CountDownLatch countDownLatch) {
        LOGGER.debug("Reading reputationScore from the cloud for the IP count :: {} ", decimalIp.size());

        final Map<Long, Integer> repCountMap = new HashMap<Long, Integer>();

        try {

            final RequestConfig defaultRequestConfig = RequestConfig.custom().setSocketTimeout(CONNECTON_TIMEOUT).setConnectTimeout(CONNECTON_TIMEOUT)
                    .setConnectionRequestTimeout(CONNECTON_TIMEOUT).build();

            final CloseableHttpClient httpclient = HttpClients.custom().setDefaultRequestConfig(defaultRequestConfig).build();

            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace(IP_URL + GETINFO);
            }

            final HttpPost httpPost = new HttpPost(IP_URL + GETINFO);
            final RepCountRequest repCountReq = new RepCountRequest(REQUESTID, OEMID, DEVICE_ID, AESEncryption.decrypt(env.getProperty("uid")));

            repCountReq.setIps(getActualIps(decimalIp));

            final String payload = objectMapper.writeValueAsString(repCountReq);

            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace(payload);
            }

            httpPost.setEntity(new StringEntity(payload, ContentType.create("application/json")));

            final CloseableHttpResponse response = httpclient.execute(httpPost);

            final RepResponse repResponse = objectMapper.readValue(response.getEntity().getContent(), RepResponse.class);

            if (repResponse != null && repResponse.getStatus() == 200) {
                for (final RepResult repResult : repResponse.getResults()) {
                    repCountMap.put(repResult.getQueries().getGetinfo().getIpint(), repResult.getQueries().getGetinfo().getReputation());
                }
            } else {
                LOGGER.warn("Unable to process the request :: Error code :: {}\n. Error code desc :: {} \n Error msg :: {}", repResponse.getError_code(),
                        repResponse.getError_code_description(), repResponse.getErrormsg());
            }

            response.close();
            httpclient.close();
        } catch (final Exception e) {
            LOGGER.error("Error while reading repcount form cloud api ::", e);
        } finally {
            countDownLatch.countDown();
            LOGGER.trace("Remaining latch Count " + countDownLatch.getCount());
        }

        return repCountMap;
    }

    public String[] getActualIps(List<Long> decimalIps) {

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Returning actual IPs from the decimalIPs ");
        }
        final String[] ips = new String[decimalIps.size()];

        int i = 0;
        for (final Long longVal : decimalIps) {
            ips[i] = CIDRUtil.getIpFromDecimal(longVal);
            i++;
        }

        return ips;
    }

    private void checkCountAndGenerateCEFRecords(List<String> cefRecords) {

        if (cefRecords.size() >= CEF_EVENT_COUNT) {
            try {
                final String fileName = env.getProperty(CEF_FOLDER) + File.separator + UUID.randomUUID().toString() + ".cef";
                final FileWriter writer = new FileWriter(fileName);
                for (final String line : cefRecords) {
                    writer.write(line + "\n");
                }
                writer.close();
                cefRecords.clear();
            } catch (final Exception e) {
                LOGGER.debug("CEF Records generation error", e);
            }
        }
    }

    private void generateCEFRecordsFromSafetySubList(List<Long> subList, List<String> cefRecords) {

        final String joinSting = StringUtils.join(subList, ",");

        final String selectQuery = "SELECT  DECIMAL_IP, RANGE, THREAT_TYPE, REP_COUNT FROM THREAT_IP_SLAVE WHERE DECIMAL_IP IN ( " + joinSting + " )";

        generateCEFRecords("remove", cefRecords, selectQuery);

    }

    private void generateCEFRecordsFromNewThreatSubList(List<Long> subList, List<String> cefRecords) {

        final String joinSting = StringUtils.join(subList, ",");

        final String selectQuery = "SELECT  DECIMAL_IP, RANGE, THREAT_TYPE, REP_COUNT FROM THREAT_IP_MASTER WHERE DECIMAL_IP IN ( " + joinSting + " )";

        generateCEFRecords("add", cefRecords, selectQuery);

    }

    private void generateCEFRecordsFromNewCatChangeSubList(List<Long> subList, List<String> cefRecords) {

        final String joinSting = StringUtils.join(subList, ",");

        final String selectQuery = "SELECT  DECIMAL_IP, RANGE, THREAT_TYPE, REP_COUNT FROM THREAT_IP_MASTER WHERE DECIMAL_IP IN ( " + joinSting + " )";

        generateCEFRecords("add", cefRecords, selectQuery);

    }

    @Override
    public void updateAPIVersionProcessed(int minorVersion) {
        try {
            executeSQLQuery(CLEAN_VERSION_TBL);

            final Map<String, Integer> params = new HashMap<String, Integer>();
            params.put("version", minorVersion);

            final String query = StrSubstitutor.replace(INSERT_VERSION_TBL, params);

            executeSQLQuery(query);
        } catch (final Exception e) {
            LOGGER.error("Error while updating the API version ", e);
        }

    }

    public void generateCEFRecords(String action, List<String> cefRecords, String selectQuery) {

        Connection connection = null;
        try {
            connection = dataSource.getConnection();

            final Statement stmt = connection.createStatement();

            final ResultSet result = stmt.executeQuery(selectQuery);

            while (result.next()) {
                final String[] addressList = CIDRUtil.getAddressList(result.getLong(1), result.getInt(2));

                for (final String ipAddress : addressList) {

                    for (final String category : BCUtil.getCategoryList(result.getInt(3))) {

                        cefRecords.add(BCUtil.getCEFFormat(ipAddress, category, result.getInt(4), CEF_VERSION, action));
                        checkCountAndGenerateCEFRecords(cefRecords);

                    }

                }
            }
            result.close();
            stmt.close();
        } catch (final SQLException e) {
            LOGGER.error("Error while exeucting the selectQuery to generate CEF records", e);
        } catch (final Exception e) {
            LOGGER.error(e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }

            } catch (final Exception e) {
                LOGGER.debug(e);
            }

        }

    }

    @Override
    public int getLastProcessedOrderNumber(int minorVersion) {
        int lastProcessedOrderNumber = 0;
        try {
            final Connection connection = dataSource.getConnection();
            final PreparedStatement ps = connection.prepareStatement(SELECT_LAST_PROCESSED_ORDER);
            ps.setInt(1, minorVersion);
            final ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                lastProcessedOrderNumber = rs.getInt(1);
            }
            rs.close();
            ps.close();
            connection.close();
        } catch (final Exception e) {
            LOGGER.error("Error while inserting into slave db", e);
        }
        return lastProcessedOrderNumber;
    }

    void printOrderNumberRecords() {

        LOGGER.info("Printing all records");
        try {
            final Connection connection = dataSource.getConnection();
            final PreparedStatement ps = connection.prepareStatement("SELECT ORDER_NUM FROM ORDER_STATUS  ");

            final ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                LOGGER.info(rs.getInt(1));
            }
            rs.close();
            ps.close();
            connection.close();
        } catch (final SQLException e) {
            LOGGER.error("Error while printing the order number ", e);
        }
    }

    void printIPUpdatesTblRecords() {

        LOGGER.info("printing all records");
        try {
            final Connection connection = dataSource.getConnection();
            final PreparedStatement ps = connection.prepareStatement("SELECT ID, DECIMAL_IP, ACTION,REP_COUNT FROM IP_UPDATES ORDER BY ID ASC ");

            final ResultSet rs = ps.executeQuery();
            final Set<Long> set = new HashSet<Long>(10000);

            int i = 0;
            while (rs.next()) {
                set.add(rs.getLong(2));
                i++;
                LOGGER.info(rs.getLong(1) + " : " + rs.getLong(2) + " : " + rs.getString(3) + " : " + rs.getString(4));
            }
            LOGGER.info("Total duplicate records found in ip_updates :: {} ", i - set.size());

            rs.close();
            ps.close();
            connection.close();
            LOGGER.info("Total IPUpdates count ::{} ", getIpUpdatesCount());
        } catch (final SQLException e) {
            LOGGER.error("Error while printing ipUpdates table records", e);
        }
    }

    @Override
    public int getIpUpdatesCount() {

        return getCount(SELECT_IP_UPDATES_COUNT);
    }

    @Override
    public void updateCurrentlyProcessedOrderNumber(int minorVersion, int updateNumber) {

        try {
            if (isTodayRecordsAvailable(minorVersion)) {
                // update existing record
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Updating existing record ");
                }
                updateLastProcessedNumber(minorVersion, updateNumber);
            } else {
                // create new record
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Creating new record ");
                }
                createNewRecordInOrderStatus(minorVersion, updateNumber);
            }

        } catch (final Exception e) {
            LOGGER.error("Error while updating the order number", e);
        }
    }

    void createNewRecordInOrderStatus(int minorVersion, int updateNumber) {
        try {

            final Connection connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            final PreparedStatement ps = connection.prepareStatement(INSERT_ORDER_STATUS);
            ps.setInt(1, minorVersion);
            ps.setInt(2, updateNumber);
            ps.execute();
            connection.commit();
            ps.close();
            connection.close();
        } catch (final SQLException e) {
            LOGGER.error("Error while inserting new record in order status ", e);
        }

    }

    void updateLastProcessedNumber(int minorVersion, int updateNumber) {

        try {
            final Connection connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            final PreparedStatement ps = connection.prepareStatement(UPDATE_ORDER_STATUS);
            ps.setInt(1, updateNumber);
            ps.setInt(2, minorVersion);
            final int updateCount = ps.executeUpdate();

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Records updated count {} ", updateCount);
            }
            connection.commit();
            ps.close();
            connection.close();
        } catch (final SQLException e) {
            LOGGER.error("Error while updating processed number in db", e);
        }

    }

    boolean isTodayRecordsAvailable(int minorVersion) {

        int count = 0;
        try {

            final Connection connection = dataSource.getConnection();
            final PreparedStatement ps = connection.prepareStatement(SELECT_ORDER_STATUS_COUNT);
            ps.setInt(1, minorVersion);
            final ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                count = rs.getInt(1);
            }
            rs.close();
            ps.close();
            connection.close();
        } catch (final SQLException e) {
            LOGGER.error("Error while selecting order status record count", e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Total count in the order table ::{} ", count);
        }

        return count > 0;

    }

    public String getToday() {
        final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(new Date());
    }

    @Override
    public void cleanIpUpdates() {
        try {
            executeSQLQuery(CLEAN_IP_UPDATE_TABLE);
        } catch (final Exception e) {
            LOGGER.error("Error while cleaning IPUpdates ", e);
        }
    }

    @Override
    public void addNewIPsToMasterTable() {

        final int maxCount = getCount("SELECT COUNT(ID) FROM IP_UPDATES WHERE ACTION='+'");

        LOGGER.info("Total records found in the ip_updates with action=add  :: {} ", maxCount);

        int count = 0;

        while (count <= maxCount) {
            final List<IPData> ipDataList = getIPUpdatesRecords(
                    "SELECT DECIMAL_IP,RANGE,THREAT_TYPE,REP_COUNT,ACTION FROM IP_UPDATES WHERE ACTION='+' OFFSET 500 ROWS  FETCH NEXT 500 ROWS ONLY ", count);

            processBulkDeletionOnMaster(ipDataList);

            processBatchInsert(INSERT_MASTER_TBL, ipDataList);
            count += 500;
        }

    }

    @Override
    public void removeIPsFromMasterTable() {
        final int maxCount = getCount("SELECT COUNT(ID) FROM IP_UPDATES WHERE ACTION='-'");

        LOGGER.info("Total records found in the ip_updates with action=remove  :: {} ", maxCount);

        int count = 0;

        while (count <= maxCount) {
            final List<IPData> ipDataList = getIPUpdatesRecords(
                    "SELECT DECIMAL_IP,RANGE,THREAT_TYPE,REP_COUNT,ACTION FROM IP_UPDATES WHERE ACTION='-' OFFSET 500 ROWS  FETCH NEXT 500 ROWS ONLY ", count);

            processBulkDeletionOnMaster(ipDataList);

            count += 500;
        }

    }

    @Override
    public void processBulkDeletionOnMaster(List<IPData> ipDataList) {

        if (!ipDataList.isEmpty()) {

            LOGGER.debug("Deleting the records from master table before adding new records from ip_updates ");

            final List<Long> ipList = new ArrayList<Long>();

            for (final IPData ipData : ipDataList) {
                ipList.add(ipData.getDecimalIp());
            }

            final String joinString = StringUtils.join(ipList, ",");

            final String deleteQuery = "DELETE FROM THREAT_IP_MASTER WHERE DECIMAL_IP IN (" + joinString + ")";

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(deleteQuery);
            }

            try {
                final Connection connection = dataSource.getConnection();
                final Statement stmt = connection.createStatement();
                stmt.execute(deleteQuery);
                stmt.close();
                connection.close();
            } catch (final Exception e) {
                LOGGER.error("Error while deleting the records ", e);
            }
        }
    }

    @Override
    public String getLicenseExpiryMessage() {

        String msg = null;

        try {
            final URL localURL = new URL(LICENSE_URL);
            final HttpURLConnection localHttpURLConnection = (HttpURLConnection) localURL.openConnection();
            localHttpURLConnection.setRequestMethod("POST");
            localHttpURLConnection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            localHttpURLConnection.setRequestProperty("Content-Type", "application/json");

            final CheckLicenseRequest checkLicenseRequest = new CheckLicenseRequest();
            checkLicenseRequest.setRequestid(REQUESTID);
            checkLicenseRequest.setDeviceid(DEVICE_ID);
            checkLicenseRequest.setOemid(OEMID);
            checkLicenseRequest.setUid(LICENSE_UID);// this is hard coded value to check license

            final CheckLicense[] values = new CheckLicense[1];
            final CheckLicense checkLicense = new CheckLicense(OEMID, DEVICE_ID, AESEncryption.decrypt(env.getProperty("uid")));
            values[0] = checkLicense;
            checkLicenseRequest.setValues(values);

            final String checkLicensePayload = objectMapper.writeValueAsString(checkLicenseRequest);

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("\n {} ", checkLicensePayload);
            }

            localHttpURLConnection.setDoOutput(true);
            final DataOutputStream localDataOutputStream = new DataOutputStream(localHttpURLConnection.getOutputStream());
            localDataOutputStream.writeBytes(checkLicensePayload);
            localDataOutputStream.flush();
            localDataOutputStream.close();

            if (localHttpURLConnection.getResponseCode() == 200) {
                final BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(localHttpURLConnection.getInputStream()));
                String line = null;
                final StringBuilder buffer = new StringBuilder();
                while ((line = localBufferedReader.readLine()) != null) {
                    buffer.append(line);
                }
                final String responseJson = buffer.toString();

                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("\n" + responseJson);
                }

                final CheckLicenseResponse checkLicenseResponse = objectMapper.readValue(responseJson, CheckLicenseResponse.class);

                if (checkLicenseResponse.getStatus() == 200) {

                    final CheckLicenseEx checkLicenseEx = checkLicenseResponse.getResults().get(0).getQueries().getChecklicenseex();

                    msg = licenseStatusMessage(checkLicenseEx);

                } else {
                    msg = checkLicenseResponse.getError_code() + "_" + checkLicenseResponse.getErrormsg();
                    LOGGER.error(checkLicenseResponse.getError_code() + "_" + checkLicenseResponse.getErrormsg());
                }

            }

        } catch (final Exception e) {
            LOGGER.error("Error while licensecheck", e);
        }

        return msg;

    }

    String licenseStatusMessage(CheckLicenseEx checkLicenseEx) throws ParseException {

        String msg = null;
        if (1 == checkLicenseEx.getSuccess()) {

            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            final Date expiryDate = sdf.parse(checkLicenseEx.getExpiry_date());

            final Date todayDate = new Date();

            final long diffInMs = expiryDate.getTime() - todayDate.getTime();

            final long diffDays = diffInMs / (24 * 60 * 60 * 1000);

            if (diffDays <= 0) {
                msg = LICENSE_EXPIRY_MSG;
            } else if (diffDays == 15 || diffDays == 30 || diffDays < LICENSE_EXP_CEF_THRESHOLD) {
                msg = String.format(LICENSE_ABOUT_EXPIRY_MSG, diffDays);
            }

            if (msg != null) {
                LOGGER.info("#############[ " + msg + " ]#############");
            }

        } else {
            msg = checkLicenseEx.getErrormsg();

            if ("ExpiredCredentials".equalsIgnoreCase(msg)) {
                msg = LICENSE_EXPIRY_MSG;
            }

            LOGGER.error(checkLicenseEx.getErrormsg());
        }

        return msg;

    }

    @Override
    public boolean isProcessingStarted() {

        boolean flag = false;

        if (processCheck.get(IS_PROCESS_STARTED) != null) {
            flag = processCheck.get(IS_PROCESS_STARTED);
        }

        return flag;
    }

    @Override
    public void updateProcessingFlagToTrue() {
        processCheck.put(IS_PROCESS_STARTED, true);
    }

    @Override
    public void updateProcessingFlagToFlase() {
        processCheck.put(IS_PROCESS_STARTED, false);
    }

    public void cleanVersionTable() {

        try {
            executeSQLQuery(CLEAN_VERSION_TBL);
        } catch (final Exception e) {
            LOGGER.error("Error while cleaning the version table", e);
        }
    }

}
