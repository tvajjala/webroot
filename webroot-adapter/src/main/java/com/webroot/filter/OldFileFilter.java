package com.webroot.filter;

import java.io.File;
import java.io.FilenameFilter;

/**
 * This filter returns the processed files (.old files).
 *
 * @author ThirupathiReddy V
 *
 */
public class OldFileFilter implements FilenameFilter {

    private final String extension;

    public OldFileFilter(String extension) {
        this.extension = extension;
    }

    @Override
    public boolean accept(File dir, String name) {
        return name.toLowerCase().endsWith(extension);
    }
}
