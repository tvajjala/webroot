package com.webroot.exception;

/**
 *
 * @author ThirupathiReddy V
 *
 */
public class BrightCloudException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = -8798483968698375024L;

    public BrightCloudException() {
    }

    public BrightCloudException(String message) {
        super(message);
    }

    public BrightCloudException(Throwable cause) {
        super(cause);
    }

    public BrightCloudException(String message, Throwable cause) {
        super(message, cause);
    }

    public BrightCloudException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
