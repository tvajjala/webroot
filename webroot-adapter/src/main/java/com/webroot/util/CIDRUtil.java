package com.webroot.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <b>This Utility returns list of IpAddresses from the given CIDR format</b>
 *
 * @author ThirupathiReddy V
 *
 */
public class CIDRUtil {

    // Suppresses default constructor, ensuring non-instantiability.
    private CIDRUtil() {
        throw new AssertionError();
    }

    /**
     *
     * @param decimalFormat
     *            decimal representation of ipAddress
     * @param range
     *            ipAddressRage
     * @return listOfIpAddress
     */
    public static String[] getAddressList(long decimalFormat, int range) {
        final String subnet = getIpFromDecimal(decimalFormat) + "/" + range;

        return getAddressList(subnet);
    }

    /**
     *
     * @param subnet
     *            CIDR notation of the ipAdress and range
     * @return list of IpAddress
     */
    private static String[] getAddressList(String subnet) {
        final SubnetUtils utils = new SubnetUtils(subnet);
        utils.setInclusiveHostCount(true);
        return utils.getInfo().getAllAddresses();
    }

    public static String getCIDRFormat(Long decimalFormat, int range) {

        return (decimalFormat.longValue() >> 24 & 0xFF) + "." + (decimalFormat.longValue() >> 16 & 0xFF) + "." + (decimalFormat.longValue() >> 8 & 0xFF) + "."
                + (decimalFormat.longValue() & 0xFF) + "/" + range;

    }

    public static String getIpFromDecimal(Long decimalFormat) {

        return (decimalFormat.longValue() >> 24 & 0xFF) + "." + (decimalFormat.longValue() >> 16 & 0xFF) + "." + (decimalFormat.longValue() >> 8 & 0xFF) + "."
                + (decimalFormat.longValue() & 0xFF);

    }

    /**
     * public static void main(String[] args) {
     *
     * System.out.println(getIpFromDecimal(16810414l));
     *
     * System.out.println(Arrays.asList(getAddressList(32265407l, 30))); }
     */

    /**
     *
     * @param ipAddress
     * @return decimalRepresentation
     */
    public long getDecimalFromIp(String ipAddress) {
        final String[] ipAddressInArray = ipAddress.split("\\.");

        long decimalFormat = 0;

        for (int i = 3; i >= 0; i--) {

            final long ipPart = Long.parseLong(ipAddressInArray[3 - i]);

            // left shifting 24,16,8,0 and bitwise OR
            // 1. 192 << 24
            // 1. 168 << 16
            // 1. 232 << 8
            // 1. 80 << 0
            final long tempResult = ipPart << i * 8;
            decimalFormat += tempResult;
        }

        return decimalFormat;

    }
}

/**
 *
 * This Utility is from apache common-net using with apache public license
 *
 */
class SubnetUtils {

    /**
     * Convenience container for subnet summary information.
     *
     */
    public final class SubnetInfo {
        private SubnetInfo() {
        }

        private int address() {
            return address;
        }

        public int asInteger(String address) {
            return toInteger(address);
        }

        private int broadcast() {
            return broadcast;
        }

        public String getAddress() {
            return format(toArray(address()));
        }

        /**
         * Get the count of available addresses. Will be zero for CIDR/31 and CIDR/32 if the inclusive flag is false.
         *
         * @return the count of addresses, may be zero.
         */
        public int getAddressCount() {
            final int count = broadcast() - network() + (isInclusiveHostCount() ? 1 : -1);
            return count < 0 ? 0 : count;
        }

        public String[] getAllAddresses() {
            final int ct = getAddressCount();
            final String[] addresses = new String[ct];
            if (ct == 0) {
                return addresses;
            }
            for (int add = low(), j = 0; add <= high(); ++add, ++j) {
                addresses[j] = format(toArray(add));
            }
            return addresses;
        }

        public String getBroadcastAddress() {
            return format(toArray(broadcast()));
        }

        public String getCidrSignature() {
            return toCidrNotation(format(toArray(address())), format(toArray(netmask())));
        }

        /**
         * Return the high address as a dotted IP address. Will be zero for CIDR/31 and CIDR/32 if the inclusive flag is false.
         *
         * @return the IP address in dotted format, may be "0.0.0.0" if there is no valid address
         */
        public String getHighAddress() {
            return format(toArray(high()));
        }

        /**
         * Return the low address as a dotted IP address. Will be zero for CIDR/31 and CIDR/32 if the inclusive flag is false.
         *
         * @return the IP address in dotted format, may be "0.0.0.0" if there is no valid address
         */
        public String getLowAddress() {
            return format(toArray(low()));
        }

        public String getNetmask() {
            return format(toArray(netmask()));
        }

        public String getNetworkAddress() {
            return format(toArray(network()));
        }

        private int high() {
            return isInclusiveHostCount() ? broadcast() : broadcast() - network() > 1 ? broadcast() - 1 : 0;
        }

        private boolean isInRange(int address) {
            final int diff = address - low();
            return diff >= 0 && diff <= high() - low();
        }

        /**
         * Returns true if the parameter <code>address</code> is in the range of usable endpoint addresses for this subnet. This excludes the network and
         * broadcast adresses.
         *
         * @param address
         *            A dot-delimited IPv4 address, e.g. "192.168.0.1"
         * @return True if in range, false otherwise
         */
        public boolean isInRange(String address) {
            return isInRange(toInteger(address));
        }

        private int low() {
            return isInclusiveHostCount() ? network() : broadcast() - network() > 1 ? network() + 1 : 0;
        }

        private int netmask() {
            return netmask;
        }

        private int network() {
            return network;
        }

        /**
         * {@inheritDoc}
         *
         * @since 2.2
         */
        @Override
        public String toString() {
            final StringBuilder buf = new StringBuilder();
            buf.append("CIDR Signature:\t[").append(getCidrSignature()).append("]").append(" Netmask: [").append(getNetmask()).append("]\n")
                    .append("Network:\t[").append(getNetworkAddress()).append("]\n").append("Broadcast:\t[").append(getBroadcastAddress()).append("]\n")
                    .append("First Address:\t[").append(getLowAddress()).append("]\n").append("Last Address:\t[").append(getHighAddress()).append("]\n")
                    .append("# Addresses:\t[").append(getAddressCount()).append("]\n");
            return buf.toString();
        }
    }

    private static final String IP_ADDRESS = "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})";
    private static final String SLASH_FORMAT = IP_ADDRESS + "/(\\d{1,3})";
    private static final Pattern addressPattern = Pattern.compile(IP_ADDRESS);
    private static final Pattern cidrPattern = Pattern.compile(SLASH_FORMAT);

    private static final int NBITS = 32;
    private int netmask = 0;
    private int address = 0;
    private int network = 0;

    private int broadcast = 0;

    /** Whether the broadcast/network address are included in host count */
    private boolean inclusiveHostCount = false;

    /**
     * Constructor that takes a CIDR-notation string, e.g. "192.168.0.1/16"
     *
     * @param cidrNotation
     *            A CIDR-notation string, e.g. "192.168.0.1/16"
     * @throws IllegalArgumentException
     *             if the parameter is invalid, i.e. does not match n.n.n.n/m where n=1-3 decimal digits, m = 1-3 decimal digits in range 1-32
     */
    public SubnetUtils(String cidrNotation) {
        calculate(cidrNotation);
    }

    /**
     * Constructor that takes a dotted decimal address and a dotted decimal mask.
     *
     * @param address
     *            An IP address, e.g. "192.168.0.1"
     * @param mask
     *            A dotted decimal netmask e.g. "255.255.0.0"
     * @throws IllegalArgumentException
     *             if the address or mask is invalid, i.e. does not match n.n.n.n where n=1-3 decimal digits and the mask is not all zeros
     */
    public SubnetUtils(String address, String mask) {
        calculate(toCidrNotation(address, mask));
    }

    /*
     * Initialize the internal fields from the supplied CIDR mask
     */
    private void calculate(String mask) {
        final Matcher matcher = cidrPattern.matcher(mask);

        if (matcher.matches()) {
            address = matchAddress(matcher);

            /* Create a binary netmask from the number of bits specification /x */
            final int cidrPart = rangeCheck(Integer.parseInt(matcher.group(5)), 0, NBITS);
            for (int j = 0; j < cidrPart; ++j) {
                netmask |= 1 << 31 - j;
            }

            /* Calculate base network address */
            network = address & netmask;

            /* Calculate broadcast address */
            broadcast = network | ~netmask;
        } else {
            throw new IllegalArgumentException("Could not parse [" + mask + "]");
        }
    }

    /*
     * Convert a 4-element array into dotted decimal format
     */
    private String format(int[] octets) {
        final StringBuilder str = new StringBuilder();
        for (int i = 0; i < octets.length; ++i) {
            str.append(octets[i]);
            if (i != octets.length - 1) {
                str.append(".");
            }
        }
        return str.toString();
    }

    /**
     * Return a {@link SubnetInfo} instance that contains subnet-specific statistics
     *
     * @return new instance
     */
    public final SubnetInfo getInfo() {
        return new SubnetInfo();
    }

    /**
     * Returns <code>true</code> if the return value of {@link SubnetInfo#getAddressCount()} includes the network address and broadcast addresses.
     *
     * @since 2.2
     */
    public boolean isInclusiveHostCount() {
        return inclusiveHostCount;
    }

    /*
     * Convenience method to extract the components of a dotted decimal address and pack into an integer using a regex match
     */
    private int matchAddress(Matcher matcher) {
        int addr = 0;
        for (int i = 1; i <= 4; ++i) {
            final int n = rangeCheck(Integer.parseInt(matcher.group(i)), -1, 255);
            addr |= (n & 0xff) << 8 * (4 - i);
        }
        return addr;
    }

    /*
     * Count the number of 1-bits in a 32-bit integer using a divide-and-conquer strategy see Hacker's Delight section 5.1
     */
    int pop(int input) {
        int x = input - (input >>> 1 & 0x55555555);
        x = (x & 0x33333333) + (x >>> 2 & 0x33333333);
        x = x + (x >>> 4) & 0x0F0F0F0F;
        x = x + (x >>> 8);
        x = x + (x >>> 16);
        return x & 0x0000003F;
    }

    /*
     * Convenience function to check integer boundaries. Checks if a value x is in the range (begin,end]. Returns x if it is in range, throws an exception
     * otherwise.
     */
    private int rangeCheck(int value, int begin, int end) {
        if (value > begin && value <= end) { // (begin,end]
            return value;
        }

        throw new IllegalArgumentException("Value [" + value + "] not in range (" + begin + "," + end + "]");
    }

    /**
     * Set to <code>true</code> if you want the return value of {@link SubnetInfo#getAddressCount()} to include the network and broadcast addresses.
     *
     * @param inclusiveHostCount
     * @since 2.2
     */
    public void setInclusiveHostCount(boolean inclusiveHostCount) {
        this.inclusiveHostCount = inclusiveHostCount;
    }

    /*
     * Convert a packed integer address into a 4-element array
     */
    private int[] toArray(int val) {
        final int[] ret = new int[4];
        for (int j = 3; j >= 0; --j) {
            ret[j] |= val >>> 8 * (3 - j) & 0xff;
        }
        return ret;
    }

    /*
     * Convert two dotted decimal addresses to a single xxx.xxx.xxx.xxx/yy format by counting the 1-bit population in the mask address. (It may be better to
     * count NBITS-#trailing zeroes for this case)
     */
    private String toCidrNotation(String addr, String mask) {
        return addr + "/" + pop(toInteger(mask));
    }

    /*
     * Convert a dotted decimal format address to a packed integer format
     */
    private int toInteger(String address) {
        final Matcher matcher = addressPattern.matcher(address);
        if (matcher.matches()) {
            return matchAddress(matcher);
        } else {
            throw new IllegalArgumentException("Could not parse [" + address + "]");
        }
    }
}
