package com.webroot.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.webroot.constant.Constants;

/**
 *
 * @author ThirupathiReddy V
 *
 */
public class BCUtil {

    /** The logger */
    private static final Logger LOGGER = LogManager.getLogger(BCUtil.class);

    private static final Map<Integer, String> threatMap = new HashMap<Integer, String>();

    // Suppresses default constructor, ensuring non-instantiability.
    private BCUtil() {
        throw new AssertionError("doesn't allowed local instantiation");
    }

    static {
        threatMap.put(0, "SpamSources");
        threatMap.put(1, "WindowsExploits");
        threatMap.put(2, "WebAttacks");
        threatMap.put(3, "BotNets");//
        threatMap.put(4, "Scanners");//
        threatMap.put(5, "DenialOfService");
        threatMap.put(6, "Reputation");
        threatMap.put(7, "Phishing");//
        threatMap.put(8, "Proxy");//
        threatMap.put(9, "Network");
        threatMap.put(10, "CloudProviders");
        threatMap.put(11, "MobileThreats");
    }

    public static List<String> getCategoryList(int threatType) {

        final List<String> list = new ArrayList<String>(5);

        for (final Integer tt : getThreatTypes(threatType)) {
            list.add(threatMap.get(tt));
        }

        return list;
    }

    public static String getCEFFormat(String ipAddress, String category, Integer reputation, String version, String action) {
        return addCEFRecord(ipAddress, category, reputation, version, action);
    }

    public static String addCEFRecord(String ipAddress, String category, Integer reputation, String version, String action) {

        final StringBuilder buffer = new StringBuilder();

        buffer.append("CEF:");
        buffer.append(version);
        buffer.append("|WEBROOT|BRIGHTCLOUD|1.0|wbr_bcti_threatIntelligence");
        buffer.append("|THREAT_INTELLIGENCE|2");
        buffer.append("|src=");
        buffer.append(ipAddress);
        if (action != null && action.trim().length() > 0) {
            buffer.append(Constants.SPACE);
            buffer.append("act=");
            buffer.append(action);
        }

        buffer.append(Constants.SPACE);
        buffer.append("cat=");
        buffer.append(category);

        buffer.append(Constants.SPACE);
        buffer.append("externalId=" + Constants.WBR_PREFIX);
        buffer.append(UUID.randomUUID());
        buffer.append(Constants.SPACE);
        buffer.append("cn1=");
        buffer.append(reputation);
        buffer.append(Constants.SPACE);
        buffer.append("cs1=");
        buffer.append(getReputationRange(reputation));
        return buffer.toString();

    }

    public static String getReputationRange(Integer reputation) {

        String repRange = "00-00";

        if (reputation == null) {
            repRange = "Invalid";
        } else if (reputation < 0) {
            repRange = "Below 00";
        } else if (reputation >= 0 && reputation <= 5) {
            repRange = "00-05";
        } else if (reputation >= 6 && reputation <= 10) {
            repRange = "06-10";
        } else if (reputation >= 11 && reputation <= 15) {
            repRange = "11-15";
        } else if (reputation >= 16 && reputation <= 20) {
            repRange = "16-20";
        } else if (reputation >= 21 && reputation <= 40) {
            repRange = "21-40";
        } else if (reputation >= 41 && reputation <= 60) {
            repRange = "41-60";
        } else if (reputation >= 61 && reputation <= 80) {
            repRange = "61-80";
        } else if (reputation >= 81 && reputation <= 100) {
            repRange = "81-100";
        } else if (reputation >= 100) {
            repRange = "100-Above";
        }

        return repRange;

    }

    public static String getErrorCEFEvent(String message) {

        final boolean expiredFlag = message.contains("expired");

        final StringBuilder buffer = new StringBuilder();

        buffer.append("CEF:");
        buffer.append(Constants.CEF_VERSION);
        buffer.append("|WEBROOT|BRIGHTCLOUD|1.0|");

        if (expiredFlag) {
            buffer.append("wbr_bcti_licenseExpired|THREAT_INTELLIGENCE_LICENSE_EXPIRED|8|");
        } else {
            buffer.append("wbr_bcti_licenseWarning|THREAT_INTELLIGENCE_LICENSE_WARNING|5|");
        }

        buffer.append("msg=");
        buffer.append(message);
        return buffer.toString();
    }

    public static String getDate(String originalString) {
        try {

            final DateFormat originalFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a", Locale.ENGLISH);
            final DateFormat targetFormat = new SimpleDateFormat("MMM dd HH:mm:ss");
            final Date date = originalFormat.parse(originalString);
            return targetFormat.format(date);
        } catch (final ParseException e) {
            LOGGER.debug("Error while parsing string into date", e);
        }
        return getCurrentDate();
    }

    public static String getCurrentDate() {
        final DateFormat targetFormat = new SimpleDateFormat("MMM dd HH:mm:ss");
        return targetFormat.format(new Date());
    }

    /**
     * This algorithm extracts different threat types from the given number.<br>
     * threatTypes are represented as sum of base two numbers.<br>
     * Example: 17= 2^0+2^4= power of 0, power of 4<br>
     * ThreatType is belongs to two categories (0 & 4)
     *
     * @param threatType
     * @return list of ThreatTypes
     */
    public static List<Integer> getThreatTypes(int threatType) {
        final String binaryReverseString = new StringBuffer(Integer.toBinaryString(threatType)).reverse().toString();

        final Pattern pattern = Pattern.compile("1");
        final Matcher matcher = pattern.matcher(binaryReverseString);

        final List<Integer> list = new ArrayList<Integer>();
        while (matcher.find()) {
            list.add(matcher.start());
        }
        return list;
    }

    /**
     * public static void main(String[] args) { System.out.println(getThreatTypes(17));
     *
     * System.out.println(getCategoryList(17));
     *
     * System.out.println(getDate("10/12/2015 5:58 PM")); }
     */

}
