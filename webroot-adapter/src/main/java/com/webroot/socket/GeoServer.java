package com.webroot.socket;

import java.io.IOException;
import java.net.BindException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.webroot.Shutdown;
import com.webroot.service.BrightCloudService;

/**
 * This is simple socket server which listens on the given port. <br>
 * It takes the ipAddress as input and returns geoInformation
 *
 * @author ThirupathiReddy V
 *
 */
public class GeoServer {

    /** the logger */
    private static final Logger LOGGER = LogManager.getLogger(GeoServer.class);

    /** server socket */
    private ServerSocket server;

    @Autowired
    private BrightCloudService brightCloudService;

    public GeoServer(int port) {
        try {
            final String hostAddress = getHostAddress();

            LOGGER.info("##################################################################");
            LOGGER.info("Running geoServer on host :: " + hostAddress + "; port :: " + port);
            LOGGER.info("##################################################################");
            server = new ServerSocket(port);
        } catch (final BindException e) {
            LOGGER.trace("Error while creating socket server  ", e);
            Shutdown.stopProcess();
            try {
                server = new ServerSocket(port);
            } catch (final IOException re) {
                LOGGER.error("Error while starting server ", re);
            }

        } catch (final Exception e) {
            LOGGER.error("Unable to create geoServer", e);
        }
    }

    String getHostAddress() {
        try {
            final InetAddress host = InetAddress.getLocalHost();
            return host.getHostAddress();
        } catch (final Exception e) {
            LOGGER.debug("Error while reading host address. using default host", e);
        }
        return "localhost";
    }

    /**
     * start this method on a separate thread so that other services will run without any blocking
     */
    @PostConstruct
    public void handleConnection() {

        try {
            Thread.sleep(3000);
        } catch (final InterruptedException e) {
            LOGGER.trace("Interrupted exception", e);
        }

        try {
            new Thread() {
                @Override
                public void run() {
                    LOGGER.info("Waiting for client message...");
                    createRequestServer();
                }
            }.start();
        } catch (final Exception e) {
            LOGGER.error("TCP Server not properly started. Please restart the service.", e);
        }
    }

    void createRequestServer() {
        //
        // The server do a loop here to accept all connection initiated by the
        // client application.
        //
        while (true) {
            try {
                if (server.isClosed()) {
                    break;
                }

                final Socket socket = server.accept();
                final ConnectionHandler handler = new ConnectionHandler(socket, brightCloudService);
                handler.start();

            } catch (final IOException e) {
                LOGGER.debug("Error while processing the socket request", e);
            }
        }
    }

    /**
     * close the SocketServer and make the port free
     */
    @PreDestroy
    public void shutdown() {
        try {
            server.close();
        } catch (final Exception e) {
            LOGGER.error("Error while closing the socket", e);
        }
    }
}
