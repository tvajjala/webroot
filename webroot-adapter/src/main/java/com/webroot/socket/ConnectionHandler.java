package com.webroot.socket;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.webroot.service.BrightCloudService;
import com.webroot.vo.IPInformation;

/**
 * geoInformation request handler
 *
 * @author ThirupathiReddy V
 *
 */
public class ConnectionHandler extends Thread {

    /** logger */
    private static final Logger LOGGER = LogManager.getLogger(ConnectionHandler.class);

    /** server socket */
    private final Socket socket;

    /** brightCloud server to read geo information */
    private final BrightCloudService brightCloudService;

    public ConnectionHandler(Socket socket, BrightCloudService brightCloudService) {
        this.socket = socket;
        this.brightCloudService = brightCloudService;
    }

    @Override
    public void run() {

        //
        // Read a message sent by client application
        //
        ObjectInputStream ois = null;
        String message = null;
        try {
            ois = new ObjectInputStream(socket.getInputStream());
            message = (String) ois.readObject();

            if ("stop".equalsIgnoreCase(message)) {
                LOGGER.info("Received stop command. terminating the process ");
                System.exit(0);// make it as normal termination
            }
        } catch (final IOException ioException) {
            LOGGER.trace("Error while opening streams", ioException);
        } catch (final ClassNotFoundException e) {
            LOGGER.trace("Error while de-serializing the object ", e);
        }

        ObjectOutputStream oos = null;
        try {
            if (message == null) {
                return;
            }
            LOGGER.info("Processing the  received geoIP :: {} ", message);
            oos = new ObjectOutputStream(socket.getOutputStream());
            final IPInformation ipInformation = brightCloudService.getFullIpInformation(message);
            if (ipInformation != null) {
                oos.writeObject(ipInformation.toString());
            }
        } catch (final Exception e) {
            LOGGER.error("Error while writing Ip data to socket", e);
            try {
                oos.writeObject(e.getMessage());
            } catch (final IOException e1) {
                LOGGER.trace("Error while writing error data to socket ", e1);
            }

        } finally {

            try {
                oos.close();
                socket.close();
            } catch (final IOException e) {
                LOGGER.trace("stream closing error", e);
            }

        }

        LOGGER.info("Request processed successfully ");

    }

}
