package com.webroot.socket;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * client program which sends ipAddress to the server on specific host and port.<br>
 * and receives geoInformation for that specific IP NOTE: this class need to share to the ESM console side to invoke the process.
 *
 * @author ThirupathiReddy V
 *
 */
public class GeoClient {

    /** logger */
    private static final Logger LOGGER = LogManager.getLogger(GeoClient.class);
    public static final int SERVER_PORT = 7777;

    public static void main(String[] args) {

        new GeoClient().handleRequest(args);
    }

    public void handleRequest(String[] args) {

        try {
            //
            // Create a connection to the server socket on the server application
            //
            String hostAddress = new GeoClient().resolveAndGetHost();

            String geoIP = null;

            if (args.length == 0) {
                System.out.println("Help Command");
                System.out.println("$/>java GeoClient server-host  ipTofindGeoInfo");
                System.out.println("$/>java GeoClient localhost 198.170.109.11");
                System.out.println(" OR ");
                System.out.println("$/>java GeoClient ipTofindGeoInfo");
                System.out.println("$/>java GeoClient 198.170.109.11");
                System.exit(0);
            } else if (args.length == 1) {
                geoIP = args[0];
            } else if (args.length == 2) {
                hostAddress = args[0];
                geoIP = args[1];
            }

            System.out.println("Webroot geoServer host :: " + hostAddress);
            System.out.println("Retrieving the geoInfo for IP :: " + geoIP);
            final Socket socket = new Socket(hostAddress, SERVER_PORT);

            //
            // Send a message to the client application
            //
            final ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject(geoIP);

            //
            // Read and display the response message sent by server application
            //
            final ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
            final String message = (String) ois.readObject();

            final String[] array = message.split(",");

            for (final String a : array) {
                System.out.println(a);
            }

            ois.close();
            oos.close();
            socket.close();
        } catch (final UnknownHostException e) {
            LOGGER.error("Unknownhost exception ", e);
        } catch (final IOException e) {
            LOGGER.error("Error while reading streams ", e);
        } catch (final ClassNotFoundException e) {
            LOGGER.error("Error while converting into object ", e);
        } catch (final Exception e) {
            LOGGER.error("Error while reading IPInformation ", e);
        }

    }

    String resolveAndGetHost() {
        try {
            final InetAddress host = InetAddress.getLocalHost();
            return host.getHostAddress();
        } catch (final Exception e) {
            LOGGER.trace("Using default host ", e);
        }

        return "localhost";
    }
}
