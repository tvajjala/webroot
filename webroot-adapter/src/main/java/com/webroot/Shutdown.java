package com.webroot;

import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This class will send the STOP command to the GeoServer class to terminate the existing process.
 *
 * @author ThirupathiReddy V
 *
 */
public class Shutdown {

    /** the logger */
    private static final Logger LOGGER = LogManager.getLogger(Shutdown.class);
    private static final int SERVER_PORT = 7777;// don't expose port outside

    private Shutdown() {

    }

    public static void stopProcess() {

        try {
            final String hostAddress = resolveAndGetHost();
            final Socket socket = new Socket(hostAddress, SERVER_PORT);
            final ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject("stop");
            oos.close();
            socket.close();
        } catch (final Exception e) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("Error while stopping the server.", e);
            }
        }
    }

    static String resolveAndGetHost() {

        String hostAddress = "localhost";

        try {
            final InetAddress host = InetAddress.getLocalHost();
            hostAddress = host.getHostAddress();
        } catch (final UnknownHostException e) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("Using host as localhost due to exception", e);
            }
        }

        return hostAddress;
    }

    public static void main(String[] args) {
        stopProcess();
    }

}
