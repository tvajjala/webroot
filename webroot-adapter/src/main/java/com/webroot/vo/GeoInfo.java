package com.webroot.vo;

import java.io.Serializable;

public class GeoInfo implements Serializable {

    private static final long serialVersionUID = 7695964103157507296L;

    private String country;
    private String region;
    private String state;

    private String city;
    private String latitude;
    private String longitude;
    private String organization;
    private String carrier;

    private String ipAddress;

    private long lastupdated;

    private String tld;

    private String sld;

    private String asn;

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setLastupdated(long lastupdated) {
        this.lastupdated = lastupdated;
    }

    public long getLastupdated() {
        return lastupdated;
    }

    public String getAsn() {
        return asn;
    }

    public String getCarrier() {
        return carrier;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getOrganization() {
        return organization;
    }

    public String getRegion() {
        return region;
    }

    public String getSld() {
        return sld;
    }

    public String getState() {
        return state;
    }

    public String getTld() {
        return tld;
    }

    public void setAsn(String asn) {
        this.asn = asn;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setSld(String sld) {
        this.sld = sld;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setTld(String tld) {
        this.tld = tld;
    }

    @Override
    public String toString() {
        return "country=" + country + ", region=" + region + ", state=" + state + ", city=" + city + ", latitude=" + latitude + ", longitude=" + longitude
                + ", organization=" + organization + ", carrier=" + carrier + ", tld=" + tld + ", sld=" + sld + ", asn=" + asn + "";
    }

}
