package com.webroot.vo;

import java.io.Serializable;

public class GeoQueries implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2331354959857984239L;

    private GeoInfo getgeoinfo;

    private RepInfo getinfo;

    public void setGetinfo(RepInfo getinfo) {
        this.getinfo = getinfo;
    }

    public RepInfo getGetinfo() {
        return getinfo;
    }

    public GeoInfo getGetgeoinfo() {
        return getgeoinfo;
    }

    public void setGetgeoinfo(GeoInfo getgeoinfo) {
        this.getgeoinfo = getgeoinfo;
    }

}
