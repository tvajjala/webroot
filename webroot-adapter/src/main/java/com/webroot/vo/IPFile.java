package com.webroot.vo;

import java.io.Serializable;

public class IPFile implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7806030742277141662L;

    private int order;
    private String name;
    private String md5;
    private String location;

    public String getLocation() {
        return location;
    }

    public String getMd5() {
        return md5;
    }

    public String getName() {
        return name;
    }

    public int getOrder() {
        return order;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    @Override
    public String toString() {
        return "BCFile [order=" + order + ", name=" + name + ", md5=" + md5 + "]";
    }

}
