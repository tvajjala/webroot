package com.webroot.vo;

import java.io.Serializable;

public class IPData implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3220407313529403905L;
    private long decimalIp;
    private int range;
    private int threatType;
    private int repCount;
    // this needs to change to char
    private String action;

    public IPData(long decimalIp, int range, int threatType, int repCount, String action) {
        this.decimalIp = decimalIp;
        this.range = range;
        this.threatType = threatType;
        this.repCount = repCount;
        this.action = action;
    }

    public IPData(long decimalIp, int range, int threatType, int repCount) {
        this.decimalIp = decimalIp;
        this.range = range;
        this.threatType = threatType;
        this.repCount = repCount;
    }

    public long getDecimalIp() {
        return decimalIp;
    }

    public void setDecimalIp(long decimalIp) {
        this.decimalIp = decimalIp;
    }

    public int getRange() {
        return range;
    }

    public void setRange(int range) {
        this.range = range;
    }

    public int getThreatType() {
        return threatType;
    }

    public void setThreatType(int threatType) {
        this.threatType = threatType;
    }

    public int getRepCount() {
        return repCount;
    }

    public void setRepCount(int repCount) {
        this.repCount = repCount;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getAction() {
        return action;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (decimalIp ^ decimalIp >>> 32);
        result = prime * result + range;
        result = prime * result + repCount;
        result = prime * result + threatType;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final IPData other = (IPData) obj;
        if (decimalIp != other.decimalIp) {
            return false;
        }
        if (range != other.range) {
            return false;
        }
        if (repCount != other.repCount) {
            return false;
        }
        if (threatType != other.threatType) {
            return false;
        }
        return true;
    }

}
