package com.webroot.vo;

import java.io.Serializable;

public class CheckLicenseResults implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6708150016682037453L;

    private String value;

    private CheckLicenseQueries queries;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public CheckLicenseQueries getQueries() {
        return queries;
    }

    public void setQueries(CheckLicenseQueries queries) {
        this.queries = queries;
    }

}
