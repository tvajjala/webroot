package com.webroot.vo;

import java.io.Serializable;

public class IPFileQueries implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8233497658395708484L;

    private IPFileGet getipfile;

    public IPFileGet getGetipfile() {
        return getipfile;
    }

    public void setGetipfile(IPFileGet getipfile) {
        this.getipfile = getipfile;
    }

}
