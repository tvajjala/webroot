package com.webroot.vo;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author ThirupathiReddy V
 *
 */
public class IPFileGet implements Serializable {

    private static final long serialVersionUID = -7531404547411651948L;

    private int file_status;
    private String file_msg;
    private String file_type;
    private int new_major;
    private int new_minor;
    private String new_md5;

    private int update_status;

    private String update_msg;

    private transient List<IPFile> ip_updates;

    private transient List<IPFile> files;

    public String getFile_msg() {
        return file_msg;
    }

    public int getFile_status() {
        return file_status;
    }

    public String getFile_type() {
        return file_type;
    }

    public List<IPFile> getFiles() {
        return files;
    }

    public List<IPFile> getIp_updates() {
        return ip_updates;
    }

    public int getNew_major() {
        return new_major;
    }

    public String getNew_md5() {
        return new_md5;
    }

    public int getNew_minor() {
        return new_minor;
    }

    public String getUpdate_msg() {
        return update_msg;
    }

    public int getUpdate_status() {
        return update_status;
    }

    public void setFile_msg(String file_msg) {
        this.file_msg = file_msg;
    }

    public void setFile_status(int file_status) {
        this.file_status = file_status;
    }

    public void setFile_type(String file_type) {
        this.file_type = file_type;
    }

    public void setFiles(List<IPFile> files) {
        this.files = files;
    }

    public void setIp_updates(List<IPFile> ip_updates) {
        this.ip_updates = ip_updates;
    }

    public void setNew_major(int new_major) {
        this.new_major = new_major;
    }

    public void setNew_md5(String new_md5) {
        this.new_md5 = new_md5;
    }

    public void setNew_minor(int new_minor) {
        this.new_minor = new_minor;
    }

    public void setUpdate_msg(String update_msg) {
        this.update_msg = update_msg;
    }

    public void setUpdate_status(int update_status) {
        this.update_status = update_status;
    }

    @Override
    public String toString() {
        return "BCGetIpFile [file_status=" + file_status + ", file_msg=" + file_msg + ", file_type=" + file_type + ", new_major=" + new_major + ", new_minor="
                + new_minor + ", new_md5=" + new_md5 + ", update_status=" + update_status + ", update_msg=" + update_msg + "]";
    }

}
