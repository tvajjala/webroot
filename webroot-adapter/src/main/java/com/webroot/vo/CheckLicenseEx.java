package com.webroot.vo;

import java.io.Serializable;

public class CheckLicenseEx implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -670608200213721745L;

    private int success;

    private String expiry_date;

    private String errormsg;

    public void setErrormsg(String errormsg) {
        this.errormsg = errormsg;
    }

    public String getErrormsg() {
        return errormsg;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public void setExpiry_date(String expiry_date) {
        this.expiry_date = expiry_date;
    }

}
