package com.webroot.vo;

import java.io.Serializable;

/**
 *
 * @author ThirupathiReddy V
 *
 */
public class RepInfo implements Serializable {

    private static final long serialVersionUID = -2235533403122840022L;

    private long ipint;

    private String domain;

    private int reputation;

    private int ip_status;

    private int threat_mask;

    private String domain_age;

    private int threat_count;

    private long current_release_date;

    private long first_release_date;

    private long last_release_date;

    public long getIpint() {
        return ipint;
    }

    public void setIpint(long ipint) {
        this.ipint = ipint;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public int getReputation() {
        return reputation;
    }

    public void setReputation(int reputation) {
        this.reputation = reputation;
    }

    public int getIp_status() {
        return ip_status;
    }

    public void setIp_status(int ip_status) {
        this.ip_status = ip_status;
    }

    public int getThreat_mask() {
        return threat_mask;
    }

    public void setThreat_mask(int threat_mask) {
        this.threat_mask = threat_mask;
    }

    public String getDomain_age() {
        return domain_age;
    }

    public void setDomain_age(String domain_age) {
        this.domain_age = domain_age;
    }

    public int getThreat_count() {
        return threat_count;
    }

    public void setThreat_count(int threat_count) {
        this.threat_count = threat_count;
    }

    public long getCurrent_release_date() {
        return current_release_date;
    }

    public void setCurrent_release_date(long current_release_date) {
        this.current_release_date = current_release_date;
    }

    public long getFirst_release_date() {
        return first_release_date;
    }

    public void setFirst_release_date(long first_release_date) {
        this.first_release_date = first_release_date;
    }

    public long getLast_release_date() {
        return last_release_date;
    }

    public void setLast_release_date(long last_release_date) {
        this.last_release_date = last_release_date;
    }

    @Override
    public String toString() {
        return String
                .format("RepInfo [ipint=%s, domain=%s, reputation=%s, ip_status=%s, threat_mask=%s, domain_age=%s, threat_count=%s, current_release_date=%s, first_release_date=%s, last_release_date=%s]",
                        ipint, domain, reputation, ip_status, threat_mask, domain_age, threat_count, current_release_date, first_release_date,
                        last_release_date);
    }

}
