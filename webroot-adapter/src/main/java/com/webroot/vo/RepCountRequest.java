package com.webroot.vo;

import java.io.Serializable;

/**
 *
 * @author ThirupathiReddy V
 *
 */
public class RepCountRequest implements Serializable {

    private static final long serialVersionUID = 5283673197281827709L;

    private String requestid;

    private String oemid;

    private String deviceid;

    private String uid;

    private String[] ips;

    private String[] queries = { "getinfo" };

    private int nocache = 1;

    private int xml = 0;

    public RepCountRequest() {

    }

    public RepCountRequest(String requestid, String oemid, String deviceid, String uid) {
        super();
        this.requestid = requestid;
        this.oemid = oemid;
        this.deviceid = deviceid;
        this.uid = uid;
    }

    public void setIps(String[] ips) {
        this.ips = ips;
    }

    public String[] getIps() {
        return ips;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public int getNocache() {
        return nocache;
    }

    public String getOemid() {
        return oemid;
    }

    public String[] getQueries() {
        return queries;
    }

    public String getRequestid() {
        return requestid;
    }

    public String getUid() {
        return uid;
    }

    public int getXml() {
        return xml;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public void setNocache(int nocache) {
        this.nocache = nocache;
    }

    public void setOemid(String oemid) {
        this.oemid = oemid;
    }

    public void setQueries(String[] queries) {
        this.queries = queries;
    }

    public void setRequestid(String requestid) {
        this.requestid = requestid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public void setXml(int xml) {
        this.xml = xml;
    }

}
