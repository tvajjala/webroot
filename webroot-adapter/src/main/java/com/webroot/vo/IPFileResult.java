package com.webroot.vo;

import java.io.Serializable;

public class IPFileResult implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 246064671612523118L;

    private String localdb;

    private IPFileQueries queries;

    public String getLocaldb() {
        return localdb;
    }

    public IPFileQueries getQueries() {
        return queries;
    }

    public void setLocaldb(String localdb) {
        this.localdb = localdb;
    }

    public void setQueries(IPFileQueries queries) {
        this.queries = queries;
    }

}
