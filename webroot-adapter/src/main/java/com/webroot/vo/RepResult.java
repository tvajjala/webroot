package com.webroot.vo;

import java.io.Serializable;

public class RepResult implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -9214594933155790325L;

    private String ip;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    private RepQueries queries;

    public void setQueries(RepQueries queries) {
        this.queries = queries;
    }

    public RepQueries getQueries() {
        return queries;
    }

}
