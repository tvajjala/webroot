package com.webroot.vo;

import java.io.Serializable;
import java.util.List;

public class GeoResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7418751217532830056L;

    private int status;

    private String requestid;

    private String type;

    private transient List<GeoResult> results;

    // exception cases
    private String query;

    private String value;;

    private String status_description;

    private int error_code;

    private String error_code_description;

    private String errormsg;

    public String getRequestid() {
        return requestid;
    }

    public List<GeoResult> getResults() {
        return results;
    }

    public int getStatus() {
        return status;
    }

    public String getType() {
        return type;
    }

    public void setRequestid(String requestid) {
        this.requestid = requestid;
    }

    public void setResults(List<GeoResult> results) {
        this.results = results;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getStatus_description() {
        return status_description;
    }

    public void setStatus_description(String status_description) {
        this.status_description = status_description;
    }

    public int getError_code() {
        return error_code;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }

    public String getError_code_description() {
        return error_code_description;
    }

    public void setError_code_description(String error_code_description) {
        this.error_code_description = error_code_description;
    }

    public String getErrormsg() {
        return errormsg;
    }

    public void setErrormsg(String errormsg) {
        this.errormsg = errormsg;
    }

}
