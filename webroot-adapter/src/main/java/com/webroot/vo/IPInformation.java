package com.webroot.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * tested with the IP : 91.229.159.49
 *
 * @author ThirupathiReddy V
 *
 */
/**
 * http://api.bcti.brightcloud.com/1.0/ip <br>
 * { "requestid": "12345", "oemid": "hp", "deviceid": "hp_arcsight", "uid": "hparcsightes_fbd89ea13f6140b097abacb3b8dd1c28", "ips": [ "91.229.159.49" ],
 * "queries": [ "getinfo","getgeoinfo" ], "xml": 0 }
 */
public class IPInformation implements Serializable {

    private static final long serialVersionUID = 3160247191618314730L;

    private String country;

    private String region;

    private String state;

    private String city;

    private String latitude;

    private String longitude;

    private String organization;

    private String carrier;

    private String ipAddress;

    private long lastupdated;

    private String tld;

    private String sld;

    private String asn;

    private long ipint;

    private String domain;

    private int reputation;

    private int ip_status;

    private int threat_mask;

    private String domain_age;

    private int threat_count;

    private long current_release_date;

    private long first_release_date;

    private long last_release_date;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public long getLastupdated() {
        return lastupdated;
    }

    public void setLastupdated(long lastupdated) {
        this.lastupdated = lastupdated;
    }

    public String getTld() {
        return tld;
    }

    public void setTld(String tld) {
        this.tld = tld;
    }

    public String getSld() {
        return sld;
    }

    public void setSld(String sld) {
        this.sld = sld;
    }

    public String getAsn() {
        return asn;
    }

    public void setAsn(String asn) {
        this.asn = asn;
    }

    public long getIpint() {
        return ipint;
    }

    public void setIpint(long ipint) {
        this.ipint = ipint;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public int getReputation() {
        return reputation;
    }

    public void setReputation(int reputation) {
        this.reputation = reputation;
    }

    public int getIp_status() {
        return ip_status;
    }

    public void setIp_status(int ip_status) {
        this.ip_status = ip_status;
    }

    public int getThreat_mask() {
        return threat_mask;
    }

    public void setThreat_mask(int threat_mask) {
        this.threat_mask = threat_mask;
    }

    public String getDomain_age() {
        return domain_age;
    }

    public void setDomain_age(String domain_age) {
        this.domain_age = domain_age;
    }

    public int getThreat_count() {
        return threat_count;
    }

    public void setThreat_count(int threat_count) {
        this.threat_count = threat_count;
    }

    public long getCurrent_release_date() {
        return current_release_date;
    }

    public void setCurrent_release_date(long current_release_date) {
        this.current_release_date = current_release_date;
    }

    public long getFirst_release_date() {
        return first_release_date;
    }

    public void setFirst_release_date(long first_release_date) {
        this.first_release_date = first_release_date;
    }

    public long getLast_release_date() {
        return last_release_date;
    }

    public void setLast_release_date(long last_release_date) {
        this.last_release_date = last_release_date;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();

        if (country != null) {
            builder.append("country=");
            builder.append(country);
            builder.append(", ");
        }
        if (region != null) {
            builder.append("region=");
            builder.append(region);
            builder.append(", ");
        }
        if (state != null) {
            builder.append("state=");
            builder.append(state);
            builder.append(", ");
        }
        if (city != null) {
            builder.append("city=");
            builder.append(city);
            builder.append(", ");
        }
        if (latitude != null) {
            builder.append("latitude=");
            builder.append(latitude);
            builder.append(", ");
        }
        if (longitude != null) {
            builder.append("longitude=");
            builder.append(longitude);
            builder.append(", ");
        }
        if (organization != null) {
            builder.append("organization=");
            builder.append(organization);
            builder.append(", ");
        }
        if (carrier != null) {
            builder.append("carrier=");
            builder.append(carrier);
            builder.append(", ");
        }

        if (ipAddress != null) {
            builder.append("ipAddress=");
            builder.append(ipAddress);
            builder.append(", ");
        }

        builder.append(", ");
        if (tld != null) {
            builder.append("tld=");
            builder.append(tld);
            builder.append(", ");
        }
        if (sld != null) {
            builder.append("sld=");
            builder.append(sld);
            builder.append(", ");
        }
        if (asn != null) {
            builder.append("asn=");
            builder.append(asn);
            builder.append(", ");
        }
        builder.append("ipint=");
        builder.append(ipint);
        builder.append(", ");

        if (domain != null) {
            builder.append("domain=");
            builder.append(domain);
            builder.append(", ");
        }
        builder.append("reputation=");
        builder.append(reputation);
        builder.append(", ip_status=");
        builder.append(ip_status);
        builder.append(", threat_mask=");
        builder.append(threat_mask);
        builder.append(", ");

        if (domain_age != null) {
            builder.append("domain_age=");
            builder.append(domain_age);
            builder.append(", ");
        }
        builder.append("threat_count=");
        builder.append(threat_count);
        builder.append(", current_release_date=");
        builder.append(new Date(current_release_date * 1000));
        builder.append(", first_release_date=");
        builder.append(new Date(first_release_date * 1000));
        builder.append(", last_release_date=");
        builder.append(new Date(last_release_date * 1000));

        return builder.toString();
    }

}
