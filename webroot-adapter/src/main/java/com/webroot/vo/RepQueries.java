package com.webroot.vo;

import java.io.Serializable;

public class RepQueries implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4501418081163613261L;

    private RepInfo getinfo;

    public RepInfo getGetinfo() {
        return getinfo;
    }

    public void setGetinfo(RepInfo getinfo) {
        this.getinfo = getinfo;
    }

}
