package com.webroot.vo;

import java.io.Serializable;

public class GeoResult implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5497552126719885101L;

    String ip;

    GeoQueries queries;

    public String getIp() {
        return ip;
    }

    public GeoQueries getQueries() {
        return queries;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setQueries(GeoQueries queries) {
        this.queries = queries;
    }

}
