package com.webroot.aes;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This instance Decrypt the UID before preparing the Payload. same instance <br>
 * is available in the webroot-license and should match the private while encryption.
 *
 * @author ThirupathiReddy V
 *
 */
public class AESEncryption {

    /**
     * The logger
     */
    private static final Logger LOGGER = LogManager.getLogger(AESEncryption.class);

    /**
     * shared private between webroot-license and webroot-adapter NOTE: it must be 16 characters in length.
     */
    private static final String SECRET_KEY = "BrightCloud12345";// hide external access for security reason

    /**
     * Don't let anyone instantiate this class.
     */
    private AESEncryption() {

    }

    public static void main(String[] args) {

        try {
            final String text = "hparcsightes_0ecdb14f91b54db0860e44d14d8d6068";
            final String b = encrypt(text);
            LOGGER.debug(b);
            LOGGER.debug(decrypt("z4GVU4YJUw3GgzpreaD1Tmo+m1kyb6q77KCjgbIW/tzXYNv9FMqUT5B/cx54mFdQ"));
        } catch (final Exception e) {
            LOGGER.debug("Error while decryption ", e);
        }
    }

    public static String decrypt(String encoded) {

        try {
            final byte[] encrypted = Base64.decodeBase64(encoded);

            final Key aesKey = new SecretKeySpec(SECRET_KEY.getBytes(), "AES");

            final Cipher cipher = Cipher.getInstance("AES");
            // decrypt the text
            cipher.init(Cipher.DECRYPT_MODE, aesKey);
            return new String(cipher.doFinal(encrypted));

        } catch (final Exception e) {
            LOGGER.error("Decryption Failed  ", e);
        }

        return null;

    }

    public static String encrypt(String plainText) {
        try {
            final Key aesKey = new SecretKeySpec(SECRET_KEY.getBytes(), "AES");

            final Cipher cipher = Cipher.getInstance("AES");
            // encrypt the text
            cipher.init(Cipher.ENCRYPT_MODE, aesKey);
            final byte[] encrypted = cipher.doFinal(plainText.getBytes());
            return Base64.encodeBase64String(encrypted);
        } catch (final Exception e) {
            LOGGER.error("Error while encryption ", e);
        }
        return plainText;
    }
}
