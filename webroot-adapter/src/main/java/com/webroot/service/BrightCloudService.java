package com.webroot.service;

import java.io.File;
import java.io.IOException;

import com.webroot.exception.BrightCloudException;
import com.webroot.vo.GeoResponse;
import com.webroot.vo.IPFileResponse;
import com.webroot.vo.IPInformation;

/**
 * core service which connects to the brightCloud API,reads and process data.
 *
 * @author ThirupathiReddy V
 *
 */
public interface BrightCloudService {

    /**
     * start processing form the scheduler
     */
    public void startProcessing();

    public void startScheduleProcessing();

    /**
     *
     * @return IPFileResponse which contains base file location and update file locations.
     * @throws Exception
     */
    public IPFileResponse getIPFileFromBrightCloud() throws IOException;

    /**
     *
     * @param ipAddress
     * @return geoInformation
     * @throws Exception
     */
    public IPInformation getFullIpInformation(String ipAddress) throws BrightCloudException;

    /**
     * process base files and generate CEF file. it process first time and on minorVersion change. it verifies
     */
    public void processBaseFiles();

    /**
     * process updates with given frequency and generate CEF files.
     */
    public void processUpdateFiles();

    /**
     *
     * @param ips
     * @return
     * @throws Exception
     */
    public GeoResponse getGeoInformationFromBrightCloud(String[] ips) throws IOException;

    /**
     *
     * @param decimalFormat
     * @return actualIP
     */
    public String getIPAddressFromDecimal(Long decimalFormat);

    /**
     * this method useful for testing the scenarios
     *
     * @param fileLocation
     * @param minorVersion
     * @throws Exception
     */
    public void insertTestDataAsStream(File file, int minorVersion) throws IOException;

    /**
     * check the license validity
     */
    public void checkLicenseValidity();

    /**
     *
     * @return number of records in the updates table
     */
    public int getIpUpdatesCount();

    /**
     *
     * @return this method useful to cleanup the old files
     */
    public String getCEFGenerationPath();
}
