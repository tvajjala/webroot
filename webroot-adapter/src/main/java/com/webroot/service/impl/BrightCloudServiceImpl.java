package com.webroot.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.webroot.aes.AESEncryption;
import com.webroot.constant.Constants;
import com.webroot.dao.BrightCloudDao;
import com.webroot.exception.BrightCloudException;
import com.webroot.service.BrightCloudService;
import com.webroot.util.BCUtil;
import com.webroot.util.CIDRUtil;
import com.webroot.vo.GeoInfo;
import com.webroot.vo.GeoRequest;
import com.webroot.vo.GeoResponse;
import com.webroot.vo.GeoResult;
import com.webroot.vo.IPData;
import com.webroot.vo.IPFile;
import com.webroot.vo.IPFileGet;
import com.webroot.vo.IPFileRequest;
import com.webroot.vo.IPFileResponse;
import com.webroot.vo.IPFileResult;
import com.webroot.vo.IPInformation;
import com.webroot.vo.RepInfo;

/**
 *
 * @author ThirupathiReddy V
 *
 */
public class BrightCloudServiceImpl implements BrightCloudService {

    private static final Logger LOGGER = LogManager.getLogger(BrightCloudService.class);

    @Autowired
    private Environment env;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private BrightCloudDao brightCloudDao;

    @Override
    public void startProcessing() {

        final String downloadNowFlag = env.getProperty("download.immediately");
        // if the immediately download flag enabled then download otherwise download as per schedule.
        if ("1".equals(downloadNowFlag)) {

            LOGGER.info("Download now flag enabled. starting the download process");

            if (!brightCloudDao.isProcessingStarted()) {// avoid second accidental start from scheduler
                brightCloudDao.updateProcessingFlagToTrue();
                processBaseFiles();
                processUpdateFiles();
                brightCloudDao.updateProcessingFlagToFlase();
            }

        } else {
            LOGGER.info("Download now flag not enabled. download happens as per scheduled frequency");
        }
    }

    @Override
    public void startScheduleProcessing() {

        if (!brightCloudDao.isProcessingStarted()) {
            LOGGER.info("Starting scheduled processing ");
            brightCloudDao.updateProcessingFlagToTrue();
            processBaseFiles();
            processUpdateFiles();
            brightCloudDao.updateProcessingFlagToFlase();
        } else {
            LOGGER.info("Another instance already running. Ignoring this thread.");
        }
    }

    @Override
    public void processBaseFiles() {
        LOGGER.info("Started base file processing  ");

        try {
            final IPFileResponse ipFileResponse = getIPFileFromBrightCloud();

            if (ipFileResponse != null && ipFileResponse.getStatus() == 200 && ipFileResponse.getResults() != null && !ipFileResponse.getResults().isEmpty()) {

                final IPFileResult bcResult = ipFileResponse.getResults().get(0);// since we are passing 12,so always get one
                final IPFileGet ipFile = bcResult.getQueries().getGetipfile();

                final int minorVersion = ipFile.getNew_minor();

                final boolean isFirstTime = brightCloudDao.isFirstTime();
                LOGGER.info("isFirstTime ? :: " + isFirstTime);

                if (isFirstTime) {
                    processOnStartup(ipFile);
                    brightCloudDao.updateAPIVersionProcessed(minorVersion);
                }

                processBaseFileOnVersionChange(minorVersion, ipFile);

            } else if (ipFileResponse != null && ipFileResponse.getError_code() == 1031) {
                generateLicenseExpiryCEF(ipFileResponse.getError_code_description());
            } else {
                LOGGER.warn("Unable to process base file. Reason :: {} ", ipFileResponse);
            }

            LOGGER.info("### Basefile processing completed ###");

        } catch (final SocketTimeoutException ste) {
            LOGGER.error("Unable to get the response from the endpoint in {} Reason {} ", Constants.CONNECTON_TIMEOUT, ste.getMessage());
            LOGGER.trace("SocketTimeoutException ", ste);
        } catch (final Exception e) {
            LOGGER.error("Error while processing the base file {} ", e.getMessage());
            LOGGER.trace("Full trace exception", e);
        }

    }

    void processBaseFileOnVersionChange(int minorVersion, IPFileGet ipFile) {
        final boolean isVersionChanged = brightCloudDao.isVersionChanged(minorVersion);
        LOGGER.info("isVersion changed ? :: " + isVersionChanged);

        if (isVersionChanged) {
            checkLicenseValidity();
            processOnVersionChange(ipFile);
            brightCloudDao.updateAPIVersionProcessed(minorVersion);
        } else {
            LOGGER.info("There is no version change detected in the minorVersion. returning to idle state");
        }
    }

    void generateLicenseExpiryCEF(String expiredCredentials) {
        try {
            final String expiredCredentialsMessage = "ExpiredCredentials".equalsIgnoreCase(expiredCredentials) ? Constants.LICENSE_EXPIRY_MSG
                    : expiredCredentials;
            final String fileName = env.getProperty(Constants.CEF_FOLDER) + File.separator + UUID.randomUUID().toString() + ".cef";
            final FileWriter writer = new FileWriter(fileName);
            writer.write(BCUtil.getErrorCEFEvent(expiredCredentialsMessage) + "\n");
            writer.close();
            LOGGER.warn("Unable to process base file due to license expiry ");
        } catch (final Exception e) {
            LOGGER.error("Error while generating license expiry CEF event ", e);
        }

    }

    @Override
    public IPFileResponse getIPFileFromBrightCloud() throws IOException {

        final RequestConfig defaultRequestConfig = RequestConfig.custom().setSocketTimeout(Constants.CONNECTON_TIMEOUT)
                .setConnectTimeout(Constants.CONNECTON_TIMEOUT).setConnectionRequestTimeout(Constants.CONNECTON_TIMEOUT).build();

        final CloseableHttpClient httpclient = HttpClients.custom().setDefaultRequestConfig(defaultRequestConfig).build();

        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Invoking brightCloud ipfile endpoint \n {}{}", Constants.LOCALDB_URL, Constants.GETIPFILE);
        }

        final HttpPost httpPost = new HttpPost(Constants.LOCALDB_URL + Constants.GETIPFILE);
        final IPFileRequest ipFileRequest = new IPFileRequest(Constants.REQUESTID, Constants.OEMID, Constants.DEVICE_ID, AESEncryption.decrypt(env
                .getProperty("uid")));

        final String payload = objectMapper.writeValueAsString(ipFileRequest);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("\n" + payload);
        }

        httpPost.setEntity(new StringEntity(payload, ContentType.create(Constants.JSON_MEDIA)));

        final CloseableHttpResponse response = httpclient.execute(httpPost);

        final IPFileResponse ipFileResponse = objectMapper.readValue(response.getEntity().getContent(), IPFileResponse.class);

        response.close();
        httpclient.close();

        return ipFileResponse;
    }

    @Override
    public IPInformation getFullIpInformation(String ipAddress) throws BrightCloudException {

        IPInformation ipInformation = brightCloudDao.getFullIPInfoFromDB(ipAddress);

        if (ipInformation != null && ipInformation.getLastupdated() + 24 * 60 * 60 * 1000 >= System.currentTimeMillis()) {
            LOGGER.info("########## Returning cached value of the geoInformation ##########");
            return ipInformation;
        } else if (ipInformation != null) {
            LOGGER.info("Removing outdated information from database ");
            brightCloudDao.deleteGeoInfoRecord(ipAddress);
        } else {
            LOGGER.info("Cached data not available for this IP. ");
        }

        try {
            ipInformation = getIPInformationFromCloud(ipAddress);
        } catch (final IOException e) {
            throw new BrightCloudException(e);
        }

        if (ipInformation != null) {
            ipInformation.setIpAddress(ipAddress);
            LOGGER.info("Saving geoInformation into the local database ");
            brightCloudDao.saveIPInformation(ipInformation);
        }

        return ipInformation;
    }

    IPInformation getIPInformationFromCloud(String ipAddress) throws IOException, BrightCloudException {

        LOGGER.info("Invoking brightCloud to read the getgeoinfo, getinfo endpoints ");
        final CloseableHttpClient httpclient = HttpClients.createDefault();

        if (LOGGER.isInfoEnabled()) {
            LOGGER.info(Constants.IP_URL);
        }

        final HttpPost httpPost = new HttpPost(Constants.IP_URL);
        final GeoRequest geoRequest = new GeoRequest(Constants.REQUESTID, Constants.OEMID, Constants.DEVICE_ID, AESEncryption.decrypt(env.getProperty("uid")));

        final String[] ips = new String[] { ipAddress };
        geoRequest.setIps(ips);

        final String payload = objectMapper.writeValueAsString(geoRequest);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("\n" + payload);
        }

        httpPost.setEntity(new StringEntity(payload, ContentType.create(Constants.JSON_MEDIA)));

        final CloseableHttpResponse response = httpclient.execute(httpPost);

        final GeoResponse geoResponse = objectMapper.readValue(response.getEntity().getContent(), GeoResponse.class);

        final IPInformation ipInformation = new IPInformation();
        if (geoResponse != null && geoResponse.getStatus() == 200 && !geoResponse.getResults().isEmpty()) {
            LOGGER.info("Received response from brightCloud ");

            final GeoResult geoResult = geoResponse.getResults().get(0);

            final GeoInfo geoInfo = geoResult.getQueries().getGetgeoinfo();
            final RepInfo repInfo = geoResult.getQueries().getGetinfo();

            ipInformation.setAsn(geoInfo.getAsn());
            ipInformation.setCarrier(geoInfo.getCarrier());
            ipInformation.setCity(geoInfo.getCity());
            ipInformation.setCountry(geoInfo.getCountry());
            ipInformation.setCurrent_release_date(repInfo.getCurrent_release_date());
            ipInformation.setDomain(repInfo.getDomain());
            ipInformation.setDomain_age(repInfo.getDomain_age());
            ipInformation.setFirst_release_date(repInfo.getFirst_release_date());
            ipInformation.setIp_status(repInfo.getIp_status());
            ipInformation.setIpAddress(geoInfo.getIpAddress());
            ipInformation.setIpint(repInfo.getIpint());
            ipInformation.setLast_release_date(repInfo.getLast_release_date());
            ipInformation.setLastupdated(geoInfo.getLastupdated()); // This field has no significance
            ipInformation.setLatitude(geoInfo.getLatitude());
            ipInformation.setLongitude(geoInfo.getLongitude());
            ipInformation.setOrganization(geoInfo.getOrganization());
            ipInformation.setRegion(geoInfo.getRegion());
            ipInformation.setReputation(repInfo.getReputation());
            ipInformation.setSld(geoInfo.getSld());
            ipInformation.setState(geoInfo.getState());
            ipInformation.setThreat_count(repInfo.getThreat_count());
            ipInformation.setThreat_mask(repInfo.getThreat_mask());
            ipInformation.setTld(geoInfo.getTld());

        } else if (geoResponse != null && geoResponse.getError_code() == 1031) {
            LOGGER.warn(Constants.LICENSE_EXPIRY_MSG);
            throw new BrightCloudException(Constants.LICENSE_EXPIRY_MSG);

        } else {
            LOGGER.error("Unable to get the response ::  {} ", geoResponse);
            throw new BrightCloudException("Unable to get the geoInformation, Please provide valid IP address ");
        }

        return ipInformation;
    }

    @Override
    public GeoResponse getGeoInformationFromBrightCloud(String[] ips) throws IOException {

        LOGGER.info("Invoking brightCloud getgeoinfo endpoint ");

        final CloseableHttpClient httpclient = HttpClients.createDefault();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(Constants.IP_URL + Constants.GETGEOINFO);
        }

        final HttpPost httpPost = new HttpPost(Constants.IP_URL + Constants.GETGEOINFO);
        final GeoRequest geoRequest = new GeoRequest(Constants.REQUESTID, Constants.OEMID, Constants.DEVICE_ID, AESEncryption.decrypt(env.getProperty("uid")));

        geoRequest.setIps(ips);

        final String payload = objectMapper.writeValueAsString(geoRequest);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(payload);
        }

        httpPost.setEntity(new StringEntity(payload, ContentType.create(Constants.JSON_MEDIA)));

        final CloseableHttpResponse response = httpclient.execute(httpPost);

        final GeoResponse geoResponse = objectMapper.readValue(response.getEntity().getContent(), GeoResponse.class);
        response.close();
        return geoResponse;
    }

    @Override
    public String getIPAddressFromDecimal(Long decimalFormat) {

        return CIDRUtil.getIpFromDecimal(decimalFormat);
    }

    @Override
    public void processUpdateFiles() {
        LOGGER.info("Processing update files ");

        final int updateIpCountCheck = brightCloudDao.getIpUpdatesCount();

        if (updateIpCountCheck > 0) {
            LOGGER.info("There is another scheduler is running to process updates. this attempt will be skipped.");
            LOGGER.warn("IpUpdates table contains some ips. make sure cleanup the table if the issue persist continuosly.");
            return;
        }

        try {

            final IPFileResponse iResponse = getIPFileFromBrightCloud();

            if (iResponse.getStatus() == 200 && iResponse.getResults() != null && !iResponse.getResults().isEmpty()) {

                final IPFileResult bcResult = iResponse.getResults().get(0);// since we are passing 12,so always get one
                final IPFileGet bcGetIpFile = bcResult.getQueries().getGetipfile();

                final int minorVersion = bcGetIpFile.getNew_minor();

                final List<IPFile> ipUpdatesList = bcGetIpFile.getIp_updates();

                processIPUpdates(ipUpdatesList, minorVersion);
                LOGGER.info("### Processing the update files completed ###");

            } else if (iResponse.getError_code() == 1031) {
                LOGGER.warn(iResponse.getError_code_description());
                generateLicenseExpiryCEFEvent();
            } else {
                LOGGER.warn("Unable to process update file. Reason :: {} ", iResponse);
            }

        } catch (final SocketTimeoutException se) {
            LOGGER.error("Unable to get the response from the endpoint in {} Reason {} ", Constants.CONNECTON_TIMEOUT, se.getMessage());
            LOGGER.error("SocketTimeoutException ", se);
        } catch (final Exception e) {
            LOGGER.error("Error while processing the update files {} ", e.getMessage());
            LOGGER.trace("Update file processing error ", e);
        }
    }

    void generateLicenseExpiryCEFEvent() {

        try {
            final String fileName = env.getProperty(Constants.CEF_FOLDER) + File.separator + UUID.randomUUID().toString() + ".cef";
            final FileWriter writer = new FileWriter(fileName);
            writer.write(BCUtil.getErrorCEFEvent(Constants.LICENSE_EXPIRY_MSG) + "\n");
            writer.close();
            LOGGER.warn("Unable to process update file due to license expiry ");
        } catch (final Exception e) {
            LOGGER.error("Error while generating license expiry CEF event ", e);
        }
    }

    void processIPUpdates(List<IPFile> ipUpdatesList, int minorVersion) {

        if (ipUpdatesList == null || ipUpdatesList.isEmpty()) {
            return;
        }

        LOGGER.info("Total updates found :: {} ", ipUpdatesList.size());

        final int lastProcessedOrder = brightCloudDao.getLastProcessedOrderNumber(minorVersion);
        LOGGER.info("Last processed order :: {} ", lastProcessedOrder);

        for (final IPFile bcFile : ipUpdatesList) {

            try {
                if (bcFile.getOrder() <= lastProcessedOrder) {
                    LOGGER.debug("Ignoring last processed update file >> {} ", bcFile.getName());
                    continue;
                }
                processSingleUpdateFile(bcFile); // it throws exception and won't execute below line

                final int updateIpCount = brightCloudDao.getIpUpdatesCount();

                if (updateIpCount > 0) {

                    updateMasterTableWithIpUpdates();

                    brightCloudDao.generateCEFsFromIpUpdates(bcFile.getOrder());// passing order number to append to file

                    LOGGER.info("Cleaning up the ip_updates table");

                    brightCloudDao.cleanIpUpdates();
                } else {
                    LOGGER.trace("There are no new events found in this update. hence not generating any CEF files");
                }
                brightCloudDao.updateCurrentlyProcessedOrderNumber(minorVersion, bcFile.getOrder());
            } catch (final Exception e) {
                LOGGER.warn("Unable to process this update  ", e);
            }

        }

    }

    public void updateMasterTableWithIpUpdates() {
        LOGGER.info("Updating master table with the ip updates information");
        brightCloudDao.addNewIPsToMasterTable();
        brightCloudDao.removeIPsFromMasterTable();
        // TODO: need to implement the scenario with action ='*'
    }

    public void processOnStartup(IPFileGet bcGetIpFile) {
        LOGGER.info("generating CEF files for the first time ");
        brightCloudDao.cleanMasterTable();

        if (bcGetIpFile.getFiles() != null) {
            for (final IPFile bcFile : bcGetIpFile.getFiles()) {
                final String locationUrl = bcFile.getLocation();
                processAndInsertIntoDB(locationUrl);
            }
        } else {
            LOGGER.warn("There are no files available to process");
        }

        brightCloudDao.generateCEFsOnStartup();
    }

    public void processOnVersionChange(IPFileGet bcGetIpFile) {

        final boolean isRecordsExists = brightCloudDao.isRecordsExistsInTheLatestTable();

        if (isRecordsExists) {
            LOGGER.info("Previous data exists in the master table. moving into slave table");
            brightCloudDao.copyDataToTheOldTable();
            LOGGER.info("Moving of data from master table to slave table is completed");
        }

        if (bcGetIpFile.getFiles() != null) {
            for (final IPFile bcFile : bcGetIpFile.getFiles()) {
                /** mostly we get one file here but as per schema it may return multiple files */
                final String locationUrl = bcFile.getLocation();
                processAndInsertIntoDB(locationUrl);
            }
        } else {
            LOGGER.warn("There are no files available to process");
        }

        final Set<Long> safeIpList = brightCloudDao.getSafeIpsList();
        final Set<Long> newThreatIpList = brightCloudDao.getNewThreatIpList();
        final Set<Long> catChangeIpList = brightCloudDao.getCategoryChangeIpList();

        brightCloudDao.generateCEFRecordsOnVersionChange(safeIpList, newThreatIpList, catChangeIpList);

    }

    public String skipHeadersAndGetDate(BufferedReader br) throws IOException {
        String line = null;
        int i = 0;
        String date = null;
        while ((line = br.readLine()) != null) {

            if (LOGGER.isInfoEnabled()) {
                LOGGER.info(">> Skipping the header : {} ", line);
            }
            if (i == 3) {// read the date header
                try {
                    date = line.replaceAll("\\]", "").split("=")[1];
                    date = BCUtil.getDate(date);
                } catch (final Exception e) {
                    LOGGER.debug("Error while skipping the headers ", e);
                }
            }
            i++;
            if (i >= 19) {
                break;// SKIP the headers
            }
        }
        return date;
    }

    public String skipRTUHeadersAndGetDate(BufferedReader br) throws IOException {
        String line = null;
        int i = 0;
        String date = null;
        while ((line = br.readLine()) != null) {

            LOGGER.trace(">> Skipping the header :{} ", line);

            if (i == 4) {// read the date header
                try {
                    date = line.replaceAll("\\]", "").split("=")[1];
                    date = BCUtil.getDate(date);
                } catch (final Exception e) {
                    LOGGER.debug("Error while skipping the headers ", e);
                }
            }
            i++;
            if (i >= 17) {
                break;// SKIP the headers
            }
        }
        return date;
    }

    private void processAndInsertIntoDB(String locationUrl) {

        LOGGER.info("Started processing base file ");

        try {

            final RequestConfig defaultRequestConfig = RequestConfig.custom().setSocketTimeout(Constants.CONNECTON_TIMEOUT)
                    .setConnectTimeout(Constants.CONNECTON_TIMEOUT).setConnectionRequestTimeout(Constants.CONNECTON_TIMEOUT).build();

            final CloseableHttpClient httpclient = HttpClients.custom().setDefaultRequestConfig(defaultRequestConfig).build();

            final HttpGet httpGet = new HttpGet(locationUrl);
            final CloseableHttpResponse httpResponse = httpclient.execute(httpGet);

            final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
            skipHeadersAndGetDate(bufferedReader);
            LOGGER.info("Records are inserting into database.Please wait it may take sometime.");
            final List<IPData> ipDataList = new ArrayList<IPData>(10000);
            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                final String[] dataArray = line.split("\t");

                ipDataList.add(new IPData(Long.parseLong(dataArray[0]), Integer.parseInt(dataArray[1]), Integer.parseInt(dataArray[2]), Integer
                        .parseInt(dataArray[3])));

                if (ipDataList.size() == Constants.CHUNK_SIZE) {
                    brightCloudDao.processBatchInsert(Constants.INSERT_MASTER_TBL, ipDataList);
                    ipDataList.clear();
                }
            }

            if (!ipDataList.isEmpty()) {
                brightCloudDao.processBatchInsert(Constants.INSERT_MASTER_TBL, ipDataList);
                ipDataList.clear();
            }
            LOGGER.info("Inserting into database completed.");

            bufferedReader.close();
            httpclient.close();

        } catch (final Exception e) {
            LOGGER.error("Error while processig base file ", e);
        }

    }

    @Override
    public void insertTestDataAsStream(File fileLocation, int minorVersion) throws IOException {

        final BufferedReader bufferedReader = new BufferedReader(new FileReader(fileLocation));
        skipHeadersAndGetDate(bufferedReader);

        final List<IPData> ipDataList = new ArrayList<IPData>(10000);
        String line = null;
        while ((line = bufferedReader.readLine()) != null) {
            final String[] dataArray = line.split("\t");

            ipDataList.add(new IPData(Long.parseLong(dataArray[0]), Integer.parseInt(dataArray[1]), Integer.parseInt(dataArray[2]), Integer
                    .parseInt(dataArray[3])));

            if (ipDataList.size() == Constants.CHUNK_SIZE) {
                brightCloudDao.processBatchInsert(Constants.INSERT_MASTER_TBL, ipDataList);
                ipDataList.clear();
            }
        }

        /** execute the last chunk which is less than 10000 */
        if (!ipDataList.isEmpty()) {
            brightCloudDao.processBatchInsert(Constants.INSERT_MASTER_TBL, ipDataList);
            ipDataList.clear();// GC
        }
        LOGGER.info("Inserting into database completed ");
        bufferedReader.close();

        brightCloudDao.updateAPIVersionProcessed(minorVersion);
    }

    private void processSingleUpdateFile(IPFile bcFile) throws IOException {

        LOGGER.info("Processing the updates file {} ", bcFile.getName());
        final String locationUrl = bcFile.getLocation();

        if (locationUrl == null) {
            return;
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Downloading IPUpdates from url \n {} ", locationUrl);
        }
        final RequestConfig reqConfig = RequestConfig.custom().setSocketTimeout(Constants.CONNECTON_TIMEOUT).setConnectTimeout(Constants.CONNECTON_TIMEOUT)
                .setConnectionRequestTimeout(Constants.CONNECTON_TIMEOUT).build();

        final CloseableHttpClient httpclient = HttpClients.custom().setDefaultRequestConfig(reqConfig).build();
        final HttpGet httpGet = new HttpGet(locationUrl);
        final CloseableHttpResponse httpResponse = httpclient.execute(httpGet);

        final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
        final String buildDate = skipRTUHeadersAndGetDate(bufferedReader);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("build_date :: {} ", buildDate);
        }
        boolean isUpdatesFound = false;
        String line = null;

        final List<IPData> ipUpdatesList = new ArrayList<IPData>(5000);

        while ((line = bufferedReader.readLine()) != null) {
            isUpdatesFound = true;

            final String[] data = line.split("\t");

            if (data.length == 4) {
                try {
                    /** initially update repCount with 10 later update */
                    ipUpdatesList.add(new IPData(Long.parseLong(data[1]), Integer.parseInt(data[2]), Integer.parseInt(data[3]), 10, data[0]));

                    if (ipUpdatesList.size() == 5000) {
                        LOGGER.info("Bucket filled with 5000 entries. updating reputationScore for these entries...");
                        brightCloudDao.updateScoreFromCloud(ipUpdatesList);
                        ipUpdatesList.clear();
                    }
                } catch (final Exception e) {
                    LOGGER.error("Error occured while splitting the update event {}", line);
                }
            } else {
                LOGGER.warn("Received wrong update event {}", line);
            }
        }

        if (!ipUpdatesList.isEmpty()) {
            LOGGER.info("Updating reputationScore for the Chunk of updates ");
            brightCloudDao.updateScoreFromCloud(ipUpdatesList);
            ipUpdatesList.clear();
        }

        if (isUpdatesFound) {
            LOGGER.info("There are some updates found in this order. Inserted into database.");
        }

        bufferedReader.close();
        httpclient.close();
    }

    @Override
    public void checkLicenseValidity() {
        try {
            final String msg = brightCloudDao.getLicenseExpiryMessage();

            if (msg != null) {
                final String licenseExpiryCEFEvent = BCUtil.getErrorCEFEvent(msg);
                final String fileName = env.getProperty(Constants.CEF_FOLDER) + File.separator + UUID.randomUUID().toString() + ".cef";
                final FileWriter writer = new FileWriter(fileName);
                writer.write(licenseExpiryCEFEvent + "\n");
                writer.close();
            }

        } catch (final Exception e) {
            LOGGER.error("Error while checking the license ", e);
        }
    }

    @Override
    public int getIpUpdatesCount() {

        return brightCloudDao.getIpUpdatesCount();
    }

    @Override
    public String getCEFGenerationPath() {
        return env.getProperty(Constants.CEF_FOLDER);
    }
}
