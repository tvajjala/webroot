package com.webroot;

import java.io.File;

import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.quartz.CronExpression;
import org.quartz.JobDataMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.jolbox.bonecp.BoneCPDataSource;
import com.webroot.dao.BrightCloudDao;
import com.webroot.dao.impl.BrightCloudDaoImpl;
import com.webroot.job.BrightCloudJob;
import com.webroot.job.OldFileCleanUpJob;
import com.webroot.service.BrightCloudService;
import com.webroot.service.impl.BrightCloudServiceImpl;
import com.webroot.socket.GeoServer;

/**
 * This class is the entry point for the application. It creates spring container and injects required beans.<br>
 * This class also loads log configuration file from the the system property <b>loggerLocation</b>.<br>
 *
 * It creates following spring beans:
 * <ul>
 * <li><b>GeoServer : </b>To process the geoLocation information</li>
 * <li><b>BrightCloudDao:</b> It talks to Derby Database</li>
 * <li><b>BrightCloudService: </b>Service layer exposes requires method can be invoked from this class</li>
 * <li><b>Quartz Schedulers: </b>These schedulers trigger the service methods and executes business logic</li>
 * </ul>
 *
 *
 * @author ThirupathiReddy V
 *
 */
@Configuration
@PropertySource(ignoreResourceNotFound = true, value = { "classpath:connector.properties", "file:connector.properties" })
// @EnableAutoConfiguration
// @EnableConfigurationProperties
// @ComponentScan(basePackages="com.webroot")
public class WebrootLauncher {

    /**
     * This static block logger configuration dynamically from the system property.<br>
     * Using this we can change the Log level in the production.<br>
     * NOTE: Restarting of the application is mandatory
     */
    static {
        try {
            final LoggerContext context = (org.apache.logging.log4j.core.LoggerContext) LogManager.getContext(false);
            final String logFile = System.getProperty("loggerLocation");
            if (logFile != null && new File(logFile).exists() && logFile.endsWith("log4j2.xml")) {
                final File file = new File(System.getProperty("loggerLocation"));
                context.setConfigLocation(file.toURI());
            }
        } catch (final Exception e) {
            LogManager.getLogger(WebrootLauncher.class).debug("Error while customizing logger ", e);
        }
    }

    /** the logger */
    private static final Logger LOGGER = LogManager.getLogger(WebrootLauncher.class);

    @Autowired
    private Environment env;

    @Bean
    public BrightCloudService brightCloudService() {
        return new BrightCloudServiceImpl();
    }

    @Bean
    public BrightCloudDao brightCloudDao() {
        return new BrightCloudDaoImpl();
    }

    @Bean
    public DataSource dataSource() {
        // instantiate, configure and return production DataSource
        final BoneCPDataSource dataSource = new BoneCPDataSource();
        dataSource.setDriverClass(org.apache.derby.jdbc.EmbeddedDriver.class.getName());
        dataSource.setJdbcUrl("jdbc:derby:" + env.getProperty("cef.folder") + "/webrootDB;create=true");
        dataSource.setIdleConnectionTestPeriodInMinutes(1);
        dataSource.setCloseOpenStatements(true);
        dataSource.setIdleMaxAgeInMinutes(1);
        dataSource.setMaxConnectionsPerPartition(100);
        dataSource.setMinConnectionsPerPartition(50);
        dataSource.setPartitionCount(1);
        dataSource.setAcquireIncrement(50);
        return dataSource;
    }

    /**
     * This class used to generate JSON Payloads
     *
     * @return objectMapper
     */
    @Bean
    public ObjectMapper objectMapper() {
        final ObjectMapper objectMapper = new ObjectMapper();
        // to print formatted value
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        return objectMapper;
    }

    /**
     * This is used to read GeoInformation form the BrightCloud API. as part of the integration command
     *
     * @return socketServer
     */
    @Bean
    public GeoServer geoSocket() {
        int port = 7777;

        try {
            port = Integer.parseInt(env.getProperty("geo.server.port"));
        } catch (final Exception e) {
            LOGGER.trace("Ignoring port parsing. Using default port ", e);
        }

        return new GeoServer(port);
    }

    /**
     * SchedulerFactoryBean glues together jobDetails and triggers to Configure Quartz Scheduler
     *
     * @return schedulerFactoryBean
     */
    @Bean
    public SchedulerFactoryBean schedulerFactoryBean() {

        final SchedulerFactoryBean schedulerFactoryBean = new SchedulerFactoryBean();
        schedulerFactoryBean.setJobDetails(brigthCloudJobBean().getObject(), cleanUpJobBean().getObject());
        schedulerFactoryBean.setTriggers(brigthCloudTriggerBean().getObject(), cleanUpTriggerBean().getObject());

        return schedulerFactoryBean;
    }

    /**
     *
     * @return cronTrigger
     */
    @Bean
    public CronTriggerFactoryBean brigthCloudTriggerBean() {

        final CronTriggerFactoryBean cronTriggerFactoryBean = new CronTriggerFactoryBean();
        cronTriggerFactoryBean.setJobDetail(brigthCloudJobBean().getObject());

        String cronExpression = env.getProperty("cron.expression");

        if (cronExpression == null || !CronExpression.isValidExpression(cronExpression)) {
            LOGGER.info("Invalid cron expression provided. using default value");
            cronExpression = "0 0/60 * * * ?";
        }

        LOGGER.info("Creating brightCloud trigger with cronExpression :: {} ", cronExpression);
        cronTriggerFactoryBean.setCronExpression(cronExpression);
        return cronTriggerFactoryBean;
    }

    /**
     *
     * @return jobDetails
     */
    @Bean
    public JobDetailFactoryBean brigthCloudJobBean() {
        final JobDetailFactoryBean jobDetailFactoryBean = new JobDetailFactoryBean();
        jobDetailFactoryBean.setJobClass(BrightCloudJob.class);
        final JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("brightCloudService", brightCloudService());
        jobDetailFactoryBean.setJobDataMap(jobDataMap);
        jobDetailFactoryBean.setDurability(true);
        return jobDetailFactoryBean;
    }

    @Bean
    public JobDetailFactoryBean cleanUpJobBean() {
        final JobDetailFactoryBean jobDetailFactoryBean = new JobDetailFactoryBean();
        jobDetailFactoryBean.setJobClass(OldFileCleanUpJob.class);
        final JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("brightCloudService", brightCloudService());
        jobDetailFactoryBean.setJobDataMap(jobDataMap);
        jobDetailFactoryBean.setDurability(true);
        return jobDetailFactoryBean;
    }

    @Bean
    public CronTriggerFactoryBean cleanUpTriggerBean() {

        final CronTriggerFactoryBean cronTriggerFactoryBean = new CronTriggerFactoryBean();
        cronTriggerFactoryBean.setJobDetail(cleanUpJobBean().getObject());

        String cronExpression = env.getProperty("cron.expression");

        if (cronExpression == null || !CronExpression.isValidExpression(cronExpression)) {
            LOGGER.info("Invalid cron expression provided. using default value");
            cronExpression = "0 0/60 * * * ?";
        }

        LOGGER.info("Creating cleanup trigger with cronExpression :: {} ", cronExpression);
        cronTriggerFactoryBean.setCronExpression(cronExpression);
        return cronTriggerFactoryBean;
    }

    public static void main(String[] args) {
        LOGGER.info("You can change log level using log4j2.xml from the installation location.");
        final ConfigurableApplicationContext ctx = SpringApplication.run(WebrootLauncher.class, args);
        final BrightCloudService brightCloudService = ctx.getBean(BrightCloudService.class);
        brightCloudService.startProcessing();

    }
}
