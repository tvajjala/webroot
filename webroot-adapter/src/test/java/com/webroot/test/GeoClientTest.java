package com.webroot.test;

import org.junit.AfterClass;
import org.junit.Assume;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.webroot.WebrootLauncher;
import com.webroot.socket.GeoClient;

@RunWith(Theories.class)
@Ignore
// Run this test case alone.
public class GeoClientTest {

    @DataPoints
    public static String[] data = new String[] { "198.170.109.11", "198.170.109.12", "54.191.66.68" };

    static AnnotationConfigApplicationContext aCtx;

    @BeforeClass
    public static void containerInitializer() {
        aCtx = new AnnotationConfigApplicationContext();
        aCtx.register(WebrootLauncher.class);
        aCtx.refresh();

    }

    @Theory
    public void geoIPInformationTest(String ipAddress) {
        Assume.assumeNotNull(ipAddress);
        new GeoClient().handleRequest(new String[] { ipAddress });
    }

    @AfterClass
    public static void containerDestroyer() {
        aCtx.close();
    }

}
