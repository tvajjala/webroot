package com.webroot.test;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.webroot.WebrootLauncher;
import com.webroot.dao.BrightCloudDao;
import com.webroot.service.BrightCloudService;
import com.webroot.vo.GeoResponse;
import com.webroot.vo.GeoResult;
import com.webroot.vo.IPData;
import com.webroot.vo.IPFileResponse;
import com.webroot.vo.IPFileResult;
import com.webroot.vo.IPInformation;

/**
 *
 * @author ThirupathiReddy V
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebrootLauncher.class)
@ActiveProfiles("qa")
public class BrightCloudRestServiceTest {

    @Autowired
    private BrightCloudService brightCloudService;

    @Autowired
    private BrightCloudDao brightCloudDao;

    private static final Logger LOGGER = LogManager.getLogger(BrightCloudRestServiceTest.class);

    /**
     * This is just to check download time for the ipFile endPoint (which is not actual file download time)
     *
     * @throws Exception
     */
    @Test
    public void downloadIPFileMetadataTest() throws Exception {

        final IPFileResponse fileResponse = brightCloudService.getIPFileFromBrightCloud();

        Assert.assertEquals(200, fileResponse.getStatus());

        Assert.assertNotNull(fileResponse.getResults());

        for (final IPFileResult fileResult : fileResponse.getResults()) {

            Assert.assertNotNull(fileResult.getQueries().getGetipfile().getFiles());

            LOGGER.info("Total base files count :: " + fileResult.getQueries().getGetipfile().getFiles().size());

            Assert.assertNotNull(fileResult.getQueries().getGetipfile().getIp_updates());

            LOGGER.info("Total updates found :: " + fileResult.getQueries().getGetipfile().getIp_updates().size());

        }

    }

    @Test(timeout = 60000)
    public void downloadBaseFileTimeTest() throws Exception {

        final IPFileResponse fileResponse = brightCloudService.getIPFileFromBrightCloud();

        Assert.assertEquals(200, fileResponse.getStatus());

        Assert.assertNotNull(fileResponse.getResults());

        for (final IPFileResult fileResult : fileResponse.getResults()) {

            Assert.assertNotNull(fileResult.getQueries().getGetipfile().getFiles());

            final String locationUrl = fileResult.getQueries().getGetipfile().getFiles().get(0).getLocation();

            final CloseableHttpClient httpclient = HttpClients.createDefault();
            final HttpGet httpGet = new HttpGet(locationUrl);
            final CloseableHttpResponse httpResponse = httpclient.execute(httpGet);

            Assert.assertEquals(200, httpResponse.getStatusLine().getStatusCode());

            /**
             * final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
             *
             * String line = null; while ((line = bufferedReader.readLine()) != null) { System.out.println(line); }
             *
             * bufferedReader.close();
             */
            httpclient.close();
        }

    }

    @Test
    public void geoInformationTest() throws Exception {

        final List<Long> decimalIPList = new ArrayList<Long>();
        decimalIPList.add(3756099946l);
        decimalIPList.add(3757071604l);
        decimalIPList.add(3758089774l);
        decimalIPList.add(3741170486l);

        final String[] ips = new String[decimalIPList.size()];

        int i = 0;
        for (final Long decimalIP : decimalIPList) {
            ips[i] = brightCloudService.getIPAddressFromDecimal(decimalIP);
            i++;
        }
        final GeoResponse geoResponse = brightCloudService.getGeoInformationFromBrightCloud(ips);

        Assert.assertEquals(200, geoResponse.getStatus());

        Assert.assertNotNull(geoResponse.getResults());

        for (final GeoResult geoResult : geoResponse.getResults()) {
            LOGGER.info(geoResult.getIp() + " ==> " + geoResult.getQueries().getGetgeoinfo());
        }

    }

    /**
     * this is to test decimal IP conversion
     */
    @Test
    public void decimalToIpAddressTest() {

        // NOTE : compare the result with http://www.ipaddressguide.com/ip

        final String actualIP = brightCloudService.getIPAddressFromDecimal(3756099946l);

        Assert.assertEquals("223.225.137.106", actualIP);

    }

    @Test
    public void geoInfoFromIPAddressServiceTest() throws Exception {
        // 223.240.92.244
        // 222.253.187.54
        // 223.255.230.46
        final IPInformation geoInfo = brightCloudService.getFullIpInformation("223.225.137.106");

        Assert.assertEquals("223.225.137.106", geoInfo.getIpAddress());
    }

    /**
     * testCase for geoInformation reading
     *
     * @throws Exception
     */
    @Test
    public void geoInfoSocketClientTest() throws Exception {

        String hostAddress = null;
        try {
            final InetAddress host = InetAddress.getLocalHost();
            hostAddress = host.getHostAddress();
        } catch (final Exception e) {
            hostAddress = "localhost";
        }

        final Socket socketClient = new Socket(hostAddress, 7777);

        final ObjectOutputStream oos = new ObjectOutputStream(socketClient.getOutputStream());
        oos.writeObject("223.225.137.106");
        final ObjectInputStream ois = new ObjectInputStream(socketClient.getInputStream());
        final String message = (String) ois.readObject();

        LOGGER.info(message);

        Assert.assertNotNull(message);

        ois.close();
        oos.close();
        socketClient.close();
    }

    @Test
    public void updateRepScoreTest() {
        final List<IPData> ipUpdatesList = Arrays.asList(new IPData(16814665, 32, 1, 17), new IPData(16818274, 32, 16, 19), new IPData(16819214, 32, 1, 20));
        brightCloudDao.updateScoreFromCloud(ipUpdatesList);
    }

    @Test
    public void bulkDeletationOnMasterTableTest() {
        final List<IPData> ipDatas = Arrays.asList(new IPData(16814665, 32, 1, 17), new IPData(16818274, 32, 16, 19), new IPData(16819214, 32, 1, 20));
        brightCloudDao.processBulkDeletionOnMaster(ipDatas);
    }

    /**
     * This test case stores data into database from the file system
     *
     * @throws Exception
     */
    @Test
    public void insertTestDataIntoMasterTable() throws Exception {

        brightCloudDao.cleanMasterTable();
        final ClassPathResource classPathResource = new ClassPathResource("wr_ip_All_1_800.txt");

        brightCloudService.insertTestDataAsStream(classPathResource.getFile(), 800);

        brightCloudDao.cleanMasterTable();
    }

    @Test
    @Ignore
    public void processUpdateFilesTest() {
        brightCloudDao.cleanIpUpdates();
        brightCloudService.processUpdateFiles();
    }

    @Test
    @Ignore
    public void processBaseFileTest() {
        brightCloudDao.cleanMasterTable();
        brightCloudService.processBaseFiles();
    }

}
